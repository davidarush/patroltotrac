import org.junit.Test;
import org.junit.BeforeClass;
import org.junit.Before;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;

import wyoroad.scrambler.Scrambler;
import wyoroad.scrambler.CipherText;
import wyoroad.scrambler.PlainText;
import wyoroad.wydgets.testing.DarTester;
import wyoroad.wydgets.WyRandom;
import wyoroad.wydgets.Properties;
import java.io.File;
// import java.io.FileWriter;
// import java.io.Writer;
import java.io.IOException;
// import java.io.FileOutputStream;
// import org.apache.commons.net.ftp.FTPClient;
// import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPFile;
import wyoroad.patrolToTrac.NewWorldCall;



/**
  AccTestNewWorldCall.java
  <br>Copyright 2020 State of Wyoming
  <br>Created at 2020 Jan 13
  @author David A. Rush
  */

public class AccTestNewWorldCall extends DarTester {

  @BeforeClass public static void setup() throws Exception {
  }


  @AfterClass public static void teardown() {
  }


  protected Properties props = null;


  @Test public void masterTester() throws Exception {
    verbosity = 3;
    test(new File("tyler-samples/242_2019111814040173.xml"));
    test(new File("tyler-samples/243_2019111814103698.xml"));
  }


  private void test(File file) throws Exception {
    vprintln(3, "file = " + file);
    NewWorldCall nwc = new NewWorldCall(file);
    vprintln(3, "nwc = " + nwc);
  }

  
}
