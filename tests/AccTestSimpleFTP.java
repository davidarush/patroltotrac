import org.junit.Test;
import org.junit.BeforeClass;
import org.junit.Before;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;

import wyoroad.scrambler.Scrambler;
import wyoroad.scrambler.CipherText;
import wyoroad.scrambler.PlainText;
import wyoroad.wydgets.testing.DarTester;
import wyoroad.wydgets.WyRandom;
import wyoroad.wydgets.Properties;
import java.io.File;
import java.io.IOException;
import org.apache.commons.net.ftp.FTPFile;
import wyoroad.patrolToTrac.SimpleFTP;


/**
  AccTestSimpleFTP.java
  <br>Copyright 2020 State of Wyoming
  <br>Created at 2020 Jan 13
  @author David A. Rush
  */

public class AccTestSimpleFTP extends DarTester {

  @BeforeClass public static void setup() throws Exception {
  }


  @AfterClass public static void teardown() {
  }


  protected Properties props = null;


  @Test public void masterTester() throws Exception {
    verbosity = 3;
    this.props = new Properties(new File("test.properties"));
    testSimpleFTP();
  }


  private void testSimpleFTP() throws Exception {
    vprintln(3, "testFTPGet()");
    String host = this.props.get("commons_ftp_host");
    String username = this.props.get("commons_ftp_username");
    PlainText password = null;
    password = new PlainText(Scrambler.Decrypt(new CipherText(this.props.get("commons_ftp_password"))));
    testSimpleFTP(host, username, password);
  }


  private void testSimpleFTP(String host, String username, PlainText password) throws IOException {
    SimpleFTP client = null;
    try {
      client = new SimpleFTP(host, username, password);
      vprintln(3, "connected and logged in to " + host + " as " + username);
      testSimpleFTP(client);
    } finally {
      if (client != null) {
        client.disconnect();
      } else {
        System.err.println("Warning: client is null, cannot disconnect.");
      }
    }
  }


  private void testSimpleFTP(SimpleFTP client) throws IOException {
    // transfer files
    String path = "Wyoming511/p9";
    dirList(client, path);
    testRetrieveFile(client, path, new File("incidents.json"));
    testRetrieveFile(client, path, new File("conditions.json"));
    testRetrieveFile(client, path, new File("rwis.json"));
    testRetrieveFile(client, path, new File("webcams.json"));
    if (client.changeWorkingDirectory(path)) {
      testRetrieveFile(client, new File("signs.json"));
      testRetrieveFile(client, new File("truckParking.json"));
      testRetrieveFile(client, new File("construction.json"));
    } else {
      fail("changeWorkingDirectory(" + path + ") returned false.");
    }
    testRetrieveAllFiles(client);
  }


  private void dirList(SimpleFTP client, String path) throws IOException {
    // FTPClient is open and successfully connected
    FTPFile[] files = client.listFiles(path);
    int n = 0;
    vprintln(3, "Files in " + path + ":");
    for(FTPFile file: files) {
      vprintln(3, String.format("  %3d: " + file + "\n", n));
      n++;
    }
    if (n < 1) {
      fail("Failed to find any files with dirList()");
    }
  }


  private boolean testRetrieveFile(SimpleFTP client, String path, File file) throws IOException {
    // SimpleFTP is open and successfully connected
    vprintln(3, "testRetrieveFile(client," + path + "," + file.getName());
    boolean result = client.retrieveFile(path, file);
    if (result) {
      vprintln(3, "Retrieved " + path + ", " + file);
    } else {
      fail("Failed to retrieve file " + path + File.pathSeparator + file.getName());
    }
    return result;
  }


  private boolean testRetrieveFile(SimpleFTP client, File file) throws IOException {
    // SimpleFTP is open and successfully connected
    vprintln(3, "testRetrieveFile(client," + file.getName());
    boolean result = client.retrieveFile(file);
    if (result) {
      vprintln(3, "Retrieved " + file);
    } else {
      fail("Failed to retrieve file " + file.getName());
    }
    return result;
  }


  private boolean testRetrieveAllFiles(SimpleFTP client) throws IOException {
    // Retrieve all the files in the current directory
    boolean result = false;
    if (result = client.retrieveAllFiles()) {
      vprintln(1, "Successfully retrieved all the files.");
    } else {
      fail("Failed to retrieve all the files.");
    }
    return result;
  }

}
