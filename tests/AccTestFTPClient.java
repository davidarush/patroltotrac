import org.junit.Test;
import org.junit.BeforeClass;
import org.junit.Before;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.List;

import wyoroad.wydgets.DatabaseReader;
import wyoroad.wydgets.DarUtils;
import wyoroad.scrambler.Scrambler;
import wyoroad.scrambler.CipherText;
import wyoroad.scrambler.PlainText;
// import wyoroad.wydgets.Whenstamp;
import wyoroad.wydgets.sql.SqlSelect;
// import wyoroad.wydgets.sql.SqlTableName;
// import wyoroad.wydgets.sql.SqlSequenceName;
// import wyoroad.wydgets.sql.DbField;
import wyoroad.wydgets.testing.DarTester;
import wyoroad.wydgets.WyRandom;
import wyoroad.wydgets.Properties;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.io.IOException;
import java.io.FileOutputStream;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPFile;



/**
  AccTestFTPClient.java
  <br>Copyright 2020 State of Wyoming
  <br>Created at 2020 Jan 13
  @author David A. Rush
  */

public class AccTestFTPClient extends DarTester {

  @BeforeClass public static void setup() throws Exception {
    /*
    Properties props = new Properties(new java.io.File("test.properties"));
    s_dbUrl      = props.getStr("trac_highlight_db_url");
    s_dbUsername = props.getStr("trac_highlight_db_username");
    s_dbPassword = new CipherText(props.getStr("trac_highlight_db_password"));
    s_select     = new SqlSelect(props.getStr("trac_highlight_select"));
    */
  }


  @AfterClass public static void teardown() {
    // DarUtils.close(sReader);
  }


  protected Properties props = null;


  @Test public void masterTester() throws Exception {
    verbosity = 3;
    this.props = new Properties(new File("test.properties"));
    testFTPGet();
  }


  private void testFTPGet() throws Exception {
    vprintln(3, "testFTPGet()");
    String host = this.props.get("commons_ftp_host");
    String username = this.props.get("commons_ftp_username");
    PlainText password = new PlainText(Scrambler.Decrypt(new CipherText(this.props.get("commons_ftp_password"))));
    testFTPGet(host, username, password);
  }


  private void testFTPGet(String host, String username, PlainText password) throws Exception {
    vprintln(3, "host = " + host);
    vprintln(3, "username = " + username);
    vprintln(3, "password = " + password);
    
    FTPClient ftp = new FTPClient();
    /*
    FTPClientConfig config = new FTPClientConfig();
    config.setXXX(YYY); // change required options
    // for example config.setServerTimeZoneId("Pacific/Pitcairn")
    ftp.configure(config );
    */
    boolean error = false;
    try {
      int reply;
      ftp.connect(host);
      vprintln(3, "Connected to " + host + ":");
      vprintln(3, ftp.getReplyString());

      // After connection attempt, you should check the reply code to verify
      // success.
      reply = ftp.getReplyCode();

      if(!FTPReply.isPositiveCompletion(reply)) {
        System.err.println("FTP server refused connection.");
      } else {
        if (ftp.login(username, password.toString())) {
          vprintln(3, "Logged in");
          // transfer files
          dirList(ftp, "Wyoming511/p9");
          getFile(ftp, "Wyoming511/p9", "incidents.json");
          ftp.logout();
          vprintln(3, "Logged out.");
        } else {
          System.err.println("Failed to login");
        }
      }
    } catch(IOException e) {
      error = true;
      e.printStackTrace();
    } finally {
      if(ftp.isConnected()) {
        try {
          ftp.disconnect();
        } catch(IOException ioe) {
          // do nothing
        }
      }
      // System.exit(error ? 1 : 0);
      vprintln(3, "error = " + error);
    }
  }


  private void dirList(FTPClient ftp, String path) throws IOException {
    // FTPClient is open and successfully connected
    FTPFile[] files = ftp.listFiles(path);
    int n = 0;
    System.out.println("Files: ");
    for(FTPFile file: files) {
      System.out.printf("  %3d: " + file + "\n", n);
      n++;
    }
  }


  private void getFile(FTPClient ftp, String path, String fileName) throws IOException {
    // FTPClient is open and successfully connected
    String full = path + File.separator + fileName;
    FileOutputStream fout = new FileOutputStream(fileName);;
    if (ftp.retrieveFile(full, fout)) {
      System.out.println("Successfully retrieved: " + full + " as " + fileName);
    } else {
      System.err.println("Failed to retrieve: " + full);
    }
    fout.close();
  }



}
