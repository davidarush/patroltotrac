import org.junit.Test;
import org.junit.BeforeClass;
import org.junit.Before;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;

/*
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.List;
import java.util.Map;
*/
import java.io.File;

// import org.json.JSONArray;
// import org.json.JSONObject;

import java.net.URL;
import org.json.JSONObject;


// import wyoroad.wydgets.DatabaseReader;
import wyoroad.wydgets.DarUtils;
//import wyoroad.wydgets.DbColumn;
//import wyoroad.wydgets.KeyGenerator;
//import wyoroad.wydgets.SelectsToHtml;
//import wyoroad.wydgets.DbCreds;
//import wyoroad.wydgets.Pair;
import wyoroad.wydgets.testing.DarTester;
//import wyoroad.wydgets.sql.SqlSelect;
import wyoroad.wydgets.Properties;

//import wyoroad.scrambler.Scrambler;
//import wyoroad.scrambler.CipherText;
//import wyoroad.scrambler.PlainText;

import wyoroad.wydgets.FileStuff;
import wyoroad.patrolToTrac.PatrolToTrac2;

/** JUnit test cases */

public class UnitTestPatrolToTrac2 extends DarTester {

  protected static Properties s_props = null;

  @BeforeClass
  public static void setup() throws Exception {
    s_props = new Properties(new File("patrolToTrac.properties"));
  }


  @AfterClass
  public static void teardown() {
  }


  @Test
  public void masterTester() throws Exception {
    // verbosity = 3;
    testSanitize();
    testLooksLikeHighway();
    verbosity = 3;
    testParse();
    testParse(new File("sample_20181015_080614.json"));
  }



  private void testSanitize() throws Exception {
    testSanitize("fubar", "fubar");
    testSanitize("P1234567890", "P1234567890");
    testSanitize("12345678901", "12345678901");
    testSanitize(" {phone}", " 307-760-2484");
    testSanitize("{phone}", "307-760-2484");
    testSanitize("{phone}", "(307) 760-2484");
    testSanitize(" {phone}", " (307) 760-2484");
    testSanitize("x{phone}", "x760-2484");
    testSanitize(" {phone}", " 7602484");
    testSanitize("{phone}", "1234567");
    testSanitize("12345678901", "12345678901");
    testSanitize("foo 12345678901", "foo 12345678901");
    testSanitize("foo 12345678901 bar", "foo 12345678901 bar");

    testSanitize("G2015000188", "G2015000188");
    testSanitize("foo G2015000188", "foo G2015000188");

    testSanitize("P2015002859", "P2015002859");
    testSanitize("P2015002859 bar", "P2015002859 bar");

    testSanitize("B2015000049", "B2015000049");
    testSanitize("foo B2015000049 bar", "foo B2015000049 bar");

    if (PatrolToTrac2.OBSCURE_DISPATCHER_NAMES) {
      testSanitize("** >>>> by: {dispatcher} on terminal: radio12", "** >>>> by: JANESMITH on terminal: radio12");
      testSanitize("** >>>> by: {dispatcher} on terminal: radio12", "** >>>> by: JANE A. DOE on terminal: radio12");
      testSanitize("** >>>> by: {dispatcher} on terminal: radio12", "** >>>> by: JANE A. DOE-SMITH on terminal: radio12");
      testSanitize("** &gt;&gt;&gt;&gt; by: {dispatcher} on terminal: radio12", "** &gt;&gt;&gt;&gt; by: JANE A. DOE-SMITH on terminal: radio12");
      testSanitize("** >>>> by: {dispatcher} on terminal: radio9", "** >>>> by: John Doe on terminal: radio9");
      testSanitize("** >>>> by: {dispatcher} on terminal: radio9", "** >>>> by: John Doe Jones-Smith on terminal: radio9");
      testSanitize("** >>>> Foo bar on terminal: whatever", "** >>>> Foo bar on terminal: whatever");
    }
    /*
    testSanitize("", "");
    testSanitize("", "");
    testSanitize("", "");
    */
  }


  private void testSanitize(String expected, String before) {
    String after = PatrolToTrac2.sanitize(before);
    vprintln(3, "\nexpected = " + DarUtils.qs(expected) + "\n   after = " + DarUtils.qs(after) + "\n before  = " + DarUtils.qs(before));
    assertEquals(expected, after);
  }


  private void testLooksLikeHighway() throws Exception {
    testLooksLikeHighway(true, "US 287");
    testLooksLikeHighway(true, "I 80");
    testLooksLikeHighway(true, "WY 210");
    testLooksLikeHighway(true, "WY 135");
    testLooksLikeHighway(false, "2026 BYPASS");
    testLooksLikeHighway(false, "Main Street");
  }

  private void testLooksLikeHighway(boolean expected, String name) {
    boolean result = PatrolToTrac2.looksLikeHighway(name);
    vprintln(3, "expected " + expected + ", result " + result + " for highway name " + name);
    assertEquals(expected, result);
  }

  private void testParse() throws Exception {
    String before = "<!doctype html>    <html>   <head>       <meta charset=\"UTF-8\">       <title>Dispatch Events</title>   </head>   <body> <br>   <pre>{ \"Events\":<br>[{\"Records\",0}] <pre>  <br>}</pre></body>   </html>";
    vprintln(3, "before = " + before);
    String after = PatrolToTrac2.deHTML(before);
    vprintln(3, "        123456789012345678901234");
    vprintln(3, "after = " + after);
    JSONObject jo = PatrolToTrac2.parseJSON(before);
    // 2016-06-04 05:22:12,462 (WARN,wyoroad.patrolToTrac.PatrolToTrac2,parseJSON(),526) after = { "Events":[{"Records",0}] <pre>  }
    vprintln(3, "jo = " + jo.toString(2));
  }


  private void testParse(File file) throws Exception {
    pushVerbosity(3);
    String before = FileStuff.fileToString(file);
    vprintln(3, "before = " + before);
    String after = PatrolToTrac2.deHTML(before);
    vprintln(3, "after deHTML:      2         3         4         5         6         7         8\n"
              + "12345678901234567890123456789012345678901234567890123456789012345678901234567890\n"
              + after);
    JSONObject jo = PatrolToTrac2.parseJSON(before);
    vprintln(3, "jo = " + jo.toString(2));
    popVerbosity();
  }
}
