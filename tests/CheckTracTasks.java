import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.List;
import java.util.ArrayList;

import wyoroad.wydgets.DatabaseReader;
import wyoroad.wydgets.DarUtils;
import wyoroad.wydgets.DbColumn;
import wyoroad.wydgets.KeyGenerator;
import wyoroad.wydgets.SelectsToHtml;
import wyoroad.scrambler.Scrambler;
import wyoroad.scrambler.CipherText;
import wyoroad.scrambler.PlainText;
import wyoroad.wydgets.DbCreds;
import wyoroad.wydgets.Pair;
import wyoroad.wydgets.testing.DarTester;
import wyoroad.wydgets.sql.SqlSelect;
import wyoroad.wydgets.Properties;
import wyoroad.wydgets.Whenstamp;
import java.io.File;
import java.io.Writer;
import java.io.FileWriter;
import wyoroad.wydata.TracTask;



public class CheckTracTasks extends DarTester {

  private static DatabaseReader sReader = null;

  private static final String OUTPUT_FILENAME = "patrolToTrac_selects.html";
  private static final String OUTPUT_TITLE = "Patrol To TRAC Selects";
  private static SelectsToHtml s2h = null;

  private static final boolean QUICK = true; // uses row limits to speed things along
  private static final byte[] WHP_COMMENTS_SCRAMBLER_KEY = {-96,-57,118,38,-47,14,49,-89,-108,-126,113,-95,118,56,115,15};

  public static void main(String[] args) throws Exception {
    CheckTracTasks x = new CheckTracTasks();
    x.go();
  }

  private Scrambler scrambler;

  private void go() throws Exception {
    this.scrambler = new Scrambler(WHP_COMMENTS_SCRAMBLER_KEY);

    Properties props = new Properties(new File("test.properties"));
    DatabaseReader reader = null;
    try {
      reader = getReader(props);
      SqlSelect select = new SqlSelect(props.get("trac_tasks_select"));
      List<TracTask> list = TracTask.getMany(reader, select);
      writeHTML(new File("trac_tasks.html"), list, select);
    } finally {
      DarUtils.close(reader);
    }
  }


  private DatabaseReader getReader(Properties props) throws Exception {
    DbCreds dbc = new DbCreds(props, "db_url", "db_username", "db_password");
    DatabaseReader reader = new DatabaseReader(dbc);
    reader.setTracer("CheckTracTasks");
    reader.open();
    return reader;
  }


  private void writeHTML(File file, List<TracTask> list, SqlSelect select) throws Exception {
    FileWriter fw = null;
    String title = "TRAC Tasks";
    try {
      fw = new FileWriter(file);
      fw.write("<!DOCTYPE html>\n");
      fw.write("<html>\n");
      fw.write("<head>\n");
      fw.write("<title>" + title + "</title>\n");
      fw.write("<style type=\"text/css\">\n");
      fw.write("body { font-family:Arial, sans-serif; }\n");
      fw.write("table { border-collapse:collapse; }\n");
      fw.write("table, tbody, tr, th, td { border:1px solid green; }\n");
      fw.write("ul, ol { margin:0px; }\n");
      fw.write(".small { font-size:75%; }\n");
      fw.write("li { margin:3px; border:1px solid gray; }\n");
      fw.write("span.comments { font-style:italic; font-size:90%; }\n");
      fw.write("div.comments { font-size:90%; margin-left:20px; font-style:italic; border:1px solid lightgray; }\n");
      // fw.write("pre { font-size:90%; } \n");
      fw.write("</style>\n");
      fw.write("</head>\n");
      fw.write("<body>\n");
      fw.write("<h1>" + title + "</h1>\n");

      fw.write("<p>Created: " + (new Whenstamp()).toString() + "</p>");
      fw.write("<p>Select: " + select.toString() + "</p>");
      fw.write("<p>" + list.size() + " TRAC tasks:</p>\n");
      writeList(fw, list);

      fw.write("</body>\n");
      fw.write("</html>\n");
    } finally {
      fw.close();
    }
    System.out.println("Wrote " + file);
  }

  private void writeList(Writer writer, List<TracTask> list) throws Exception {
    writer.write("<ol>\n");
    for(TracTask thing: list) {
      writeLi(writer, thing);
    }
    writer.write("</ol>\n");
  }

  private void writeLi(Writer writer, TracTask thing) throws Exception {
    writer.write("<li>\n");
    writer.write("PK = " + thing.getPk() + "\n");
    writer.write("<br>Created at " + thing.getCreatedAt() + " by " + thing.getCreatedBy() + "\n");
    writer.write("<br>Completed at " + thing.getCompletedAt() + " by " + thing.getCompletedBy() + "\n");
    writer.write("<br>Description: " + thing.getDescription() + "\n");
    writer.write("<br>Comments: ");
    if (thing.getComments() == null) {
      writer.write("none");
    } else {
      // writer.write("<span class=\"comments\">" + scrambler.decrypt(new CipherText(thing.getComments())) + "</span>\n");
      writer.write("<div class=\"comments\"><pre>" + scrambler.decrypt(new CipherText(thing.getComments())) + "</pre></div>\n");
    }
    /*
    writer.write("<br>" + thing.get() + "\n");
    writer.write("<br>" + thing.get() + "\n");
    writer.write("<br>" + thing.get() + "\n");
    writer.write("<br>" + + "\n");
    */
    writer.write("</li>\n");
  }

  private void writeTable(Writer writer, List<TracTask> list) throws Exception {
    writer.write("<table>\n");
    int n = 0;
    for(TracTask thing: list) {
      if (n % 10 == 0) {
        writer.write(thing.getTrTh());
      }
      writer.write(thing.getTrTd(n));
      n++;
    }
    writer.write("</table>\n");
  }


}
