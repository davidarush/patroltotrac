import org.junit.Test;
import org.junit.BeforeClass;
import org.junit.Before;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.List;

import wyoroad.wydgets.DatabaseReader;
import wyoroad.wydgets.DarUtils;
// import wyoroad.wydgets.KeyGenerator;
// import wyoroad.wydgets.SelectToHtml;
// import wyoroad.scrambler.Scrambler;
import wyoroad.scrambler.CipherText;
// import wyoroad.scrambler.PlainText;
// import wyoroad.wydgets.Whenstamp;
import wyoroad.wydgets.sql.SqlSelect;
// import wyoroad.wydgets.sql.SqlTableName;
// import wyoroad.wydgets.sql.SqlSequenceName;
// import wyoroad.wydgets.sql.DbField;
import wyoroad.wydgets.testing.DarTester;
import wyoroad.wydgets.WyRandom;
import wyoroad.wydgets.Properties;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;


import wyoroad.wydgets.trac.TracHighlight;
import wyoroad.wydgets.trac.TracHighlighter;

/**
  AccTestTracHighlighter.java
  <br/>Copyright 2016 State of Wyoming
  <br/>Created at 2016-07-26 10:46:47 from AccTestTracHighlight.java
  @author David A. Rush
  */

public class AccTestTracHighlighter extends DarTester {

  private static String s_dbUrl = null;
  private static String s_dbUsername = null;
  private static CipherText s_dbPassword = null;
  private static SqlSelect s_select = null;

  @BeforeClass public static void setup() throws Exception {
    Properties props = new Properties(new java.io.File("test.properties"));
    s_dbUrl      = props.getStr("trac_highlight_db_url");
    s_dbUsername = props.getStr("trac_highlight_db_username");
    s_dbPassword = new CipherText(props.getStr("trac_highlight_db_password"));
    s_select     = new SqlSelect(props.getStr("trac_highlight_select"));
  }


  @AfterClass public static void teardown() {
    // DarUtils.close(sReader);
  }

  @Test public void masterTester() throws Exception {
    // This is so I can enforce the order of tests
    verbosity = 3;
    test();
  }


  private void test() throws Exception {
    TracHighlighter x = new TracHighlighter().dbUrl(s_dbUrl).dbUsername(s_dbUsername).dbPassword(s_dbPassword).select(s_select).bake();
    List<TracHighlight> list = x.getTracHighlights();
    assertNotNull("list is null", list);
    print(3, list);
    List<DatabaseReader> openReaders = DatabaseReader.getOpenReaders();
    vprintln(3, "Found " + openReaders.size() + " open reader(s).");
    if (openReaders.size() > 0) {
      assertEquals("Left " + openReaders.size() + " reader(s) open.", 0, openReaders.size());
    }
    String allWords = null;
    boolean first = true;
    for(TracHighlight th: list) {
      if (first) {
        allWords = th.getWord();
        first = false;
      } else {
        allWords += " " + th.getWord();
      }
    }

    String[] testCases = {
      "Now is the time for all FATAL men to come to the aid of their BLOCKAGE country."
      ,"BLOCK at the start"
      ,"at the end BLOCK"
      ,"FATAL at the start and at the end FATAL"
      ,"The closed road is CLOSED at the moment."
      , allWords
    };
    FileWriter writer = null;
    try {
      File file = new File("tracHighlighter_results.html");
      writer = new FileWriter(file);
      writer.write("<ol>\n");
      for(String before: testCases) {
        test(writer, x, before);
      }
      writer.write("</ol><hr>\n<ol>\n");
      for(String before: testCases) {
        test(writer, x, "<span style=\"font-style:italic;\">" + before + "</span>");
      }
      writer.write("</ol>\n");
      vprintln(1, "Wrote " + file);
    } finally {
      writer.close();
    }
  }

  private void test(Writer writer, TracHighlighter x, String before) throws Exception {
    String after = x.highlight(before);
    vprintln(3, "Before: " + before);
    vprintln(3, " After: " + after);
    writer.write("<li>\n");
    writer.write("before: " + before + "\n");
    writer.write("<br>after: " + after + "\n");
    writer.write("</li>\n");
  }

  private void print(int verbosity, List<TracHighlight> list) {
    int n = 0;
    int longestWord = "Word".length(); // length of "Word"
    int longestTextColor = "Color".length(); // length of "Color"
    int longestTextStyle = "Style".length(); // length of "Style"
    int longestBackground = "Background".length(); // length of "Background"
    for(TracHighlight th: list) {
      longestWord = maxLen(th.getWord(), longestWord);
      longestTextColor = maxLen(th.getTextColor(), longestTextColor);
      longestTextStyle = maxLen(th.getTextStyle(), longestTextStyle);
      longestBackground = maxLen(th.getBackground(), longestBackground);
    }
    longestWord += 2; // allow for quotes
    n = 1;
    if (list.size() > 9) {
      n++;
    }
    if (list.size() > 99) {
      n++;
    }
    if (list.size() > 999) {
      n++;
    }
    String fmt = "%" + n + "d %-" + longestWord + "s %-" + longestTextColor + "s %-" + longestTextStyle + "s %-" + longestBackground + "s";
    String header = String.format("%" + n + "s %-" + longestWord + "s %-" + longestTextColor + "s %-" + longestTextStyle + "s %-" + longestBackground + "s"
                        , "N", "Word", "Color", "Style", "Background");
    vprintln(3, header);
    n = 0;
    for(TracHighlight th: list) {
      vprintln(3, String.format(fmt, n, DarUtils.qs(th.getWord()), th.getTextColor(), th.getTextStyle(), th.getBackground()));
      n++;
    }
  }

  private static int maxLen(String s, int lenBefore) {
    int lenAfter = lenBefore;
    if (s != null) {
      int len = s.length();
      if (len > lenAfter) {
        lenAfter = len;
      }
    }
    return lenAfter;
  }

}
