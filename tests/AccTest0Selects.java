import org.junit.Test;
import org.junit.BeforeClass;
import org.junit.Before;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.List;
import java.util.ArrayList;

import wyoroad.wydgets.DatabaseReader;
import wyoroad.wydgets.DarUtils;
import wyoroad.wydgets.DbColumn;
import wyoroad.wydgets.KeyGenerator;
import wyoroad.wydgets.SelectsToHtml;
// import wyoroad.scrambler.Scrambler;
// import wyoroad.scrambler.CipherText;
// import wyoroad.scrambler.PlainText;
import wyoroad.wydgets.DbCreds;
import wyoroad.wydgets.Pair;
import wyoroad.wydgets.testing.DarTester;
import wyoroad.wydgets.sql.SqlSelect;


/** JUnit test cases */

public class AccTest0Selects extends DarTester {

  private static DatabaseReader sReader = null;

  private static final String OUTPUT_FILENAME = "patrolToTrac_selects.html";
  private static final String OUTPUT_TITLE = "Patrol To TRAC Selects";
  private static SelectsToHtml s2h = null;

  private static final boolean QUICK = true; // uses row limits to speed things along


  @BeforeClass
  public static void setup() throws Exception {
    java.util.Properties props = DarUtils.getProperties("test.properties");

    DbCreds dbc = new DbCreds(props, "db_url", "db_username", "db_password");
    if (true) {
      System.out.println("dbc: " + dbc);
    }

    sReader = new DatabaseReader(dbc);
    sReader.setTracer("AccTest0Selects-patrolToTrac");
    sReader.open();

    SqlSelect select =  null;

    s2h = new SelectsToHtml();
    String name = null;

    name = "patrol_events_select";
    s2h.add(sReader, new SqlSelect(DarUtils.get(props, name)), name);

    name = "trac_tasks_select";
    s2h.add(sReader, new SqlSelect(DarUtils.get(props, name)), name);


  }

  private static String insertRownum(String before) {
    String after = before;
    int n = before.indexOf("ROWNUM");
    if (n < 0) {
      n = before.indexOf("order by");
      if (n > 0) {
        after = before.substring(0,n) + "where ROWNUM <= 100 " + before.substring(n);
      }
    }
    return after;
  }



  @AfterClass
  public static void teardown() {
    DarUtils.close(sReader);
  }

  @Test
  public void test0() throws Exception {
    verbosity = 0;
    s2h.go(OUTPUT_FILENAME, OUTPUT_TITLE);
    assertEquals(0, s2h.getExceptionCount());
  }

}
