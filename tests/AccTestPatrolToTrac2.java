import org.junit.Test;
import org.junit.BeforeClass;
import org.junit.Before;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import java.io.File;
import java.net.URL;
import wyoroad.wydgets.DarUtils;
import wyoroad.wydgets.testing.DarTester;
import wyoroad.wydgets.Properties;
import wyoroad.wydgets.FileStuff;
import wyoroad.patrolToTrac.PatrolToTrac2;

/** JUnit test cases */

public class AccTestPatrolToTrac2 extends DarTester {

  protected static Properties s_props = null;

  @BeforeClass
  public static void setup() throws Exception {
    s_props = new Properties(new File("patrolToTrac.properties"));
  }


  @AfterClass
  public static void teardown() {
  }


  @Test
  public void masterTester() throws Exception {
    verbosity = 3;
    testGetDistrict();
  }


  private void testGetDistrict() throws Exception {
    PatrolToTrac2 x = new PatrolToTrac2();
    vprintln(1, "GBFFE URI: " + x.getGbffeCNtoDistrictUri());
    testGetDistrict(1, x, "I 80", 350.0);
    testGetDistrict(1, x, "I 25", 30.0);
    testGetDistrict(2, x, "I 25", 31.0);
    // testGetDistrict(1, x, "US 87", 30.0);
    // testGetDistrict(2, x, "US 87", 31.0);
    testGetDistrict(2, x, "I 25", 247.3);
    // testGetDistrict(2, x, "WY 1002", 13.5);
    testGetDistrict(2, x, "WY 192", 3.5);
    testGetDistrict(2, x, "WY 387", 131.5);
    testGetDistrict(4, x, "WY 387", 131.9);
    testGetDistrict(0, x, "US 16", 16.0);  // 0 because of ambiguous results
    testGetDistrict(3, x, "US 191", 200.0);
    testGetDistrict(3, x, "I 80", 0.0);
    testGetDistrict(3, x, "I 80", 187.25);
    testGetDistrict(1, x, "I 80", 187.35);
  }

  private void testGetDistrict(int expected, PatrolToTrac2 x, String commonName, double refMarker) throws Exception {
    int after = x.getDistrict(commonName, refMarker);
    vprintln(3, "expected = " + expected + " after = " + after + " for " + commonName + " @ " + refMarker);
    assertEquals(expected, after);
  }

}
