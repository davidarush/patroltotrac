
//   ' by: KAYLA HEITSTUMAN on terminal: radio'


const readline = require('readline');
const fs = require('fs');

doit();
// doitFile('sample_20160516a.json');
// testit();


function doit() {
  // stdin, stdout
  const rl = readline.createInterface({
    input: process.stdin
  });

  rl.on('line', (line) => {
    console.log(fixit(line));
  });
}


function doitFile(fileName) {
  // input file, stdout
  const rl = readline.createInterface({
    input: fs.createReadStream(fileName)
  });

  rl.on('line', (line) => {
    console.log(' line:', line);
    var fixed = fixit(line);
    console.log('fixed:', fixed);
  });
}

function testit() {
  var before;
  before = '  "Comments": "| VEH VS DEER | GIRLFRIEND IS CALLING FOR RP (HER BOYFRIEND) | HE WAS 10-76 FROM RAWLINS TO CASPER, HIT THE DEER AND THEN | GOT A RIDE TO WORK AT THE SINCLAIR REFINERARY FROM A PASSERBY | HE LEFT THE VEHCILE ON THE SHOULDER OF THE ROAD, DEER IS DEAD | DRIVERS NAME FRANK SMITH 786-1111111 | RP IS DORA SMITH 307-111-1111 | VEH IS BRO 2016 TOYOTA TUNDRA WY/6-6T18039 | SPECIAL ADDRESS COMMENT: | ***WIND RIVER INDIAN RESERVATION*** | GIRLFRIEND SAID 69 ON 287 BUT 287 TURNS INTO 220 | **PER search of CAD table completed at 2016/05/17 06:04:37 | **VEH search of CAD table completed at 2016/05/17 06:04:37 | **LOI search  of CAD table completed at 2016/05/17 06:04:37 | NO RD BLKG | DEER STUCK IN THE FENDER | ** LOI information for Event # P2016066269 was viewed at: 2016/05/17 06:06:59 | ** &gt;&gt;&gt;&gt; by: JANE SMITH on terminal: radio4 | Preempt | **VEH search of CAD table completed at 2016/05/17 06:09:51 | ** LOI information for Event # P2016066269 was viewed at: 2016/05/17 06:09:56 | ** &gt;&gt;&gt;&gt; by: SALLY X. SMITH on terminal: px1 | ** LOI information for Event # P2016066269 was viewed at: 2016/05/17 06:10:06 | ** &gt;&gt;&gt;&gt; by: CATHY SMITH on terminal: radio4 | **VEH search of CAD table completed at 2016/05/17 06:10:51 | **VEH search of CAD table completed at 2016/05/17 06:10:56 | ** LOI information for Event # P2016066269 was viewed at: 2016/05/17 06:11:03 | ** &gt;&gt;&gt;&gt; by: DELLA SMITH on terminal: radio4 | **VEH search of CAD table completed at 2016/05/17 06:11:09 | **VEH search of CAD table completed at 2016/05/17 06:11:20 | ** LOI information for Event # P2016066269 was viewed at: 2016/05/17 06:11:37 | ** &gt;&gt;&gt;&gt; by: EUGENE X. SMITH on terminal: px1 | **VEH search of CAD table completed at 2016/05/17 06:12:08 | ** LOI information for Event # P2016066269 was viewed at: 2016/05/17 06:12:19 | ** &gt;&gt;&gt;&gt; by: FELICITY SMITH on terminal: px1 | ** LOI information for Event # P2016066269 was viewed at: 2016/05/17 06:13:36 | ** &gt;&gt;&gt;&gt; by: GABBY SMITH on terminal: radio4 | ** LOI information for Event # P2016066269 was viewed at: 2016/05/17 06:13:48 | ** &gt;&gt;&gt;&gt; by: HADLEY SMITH on terminal: radio4 | ** LOI information for Event # P2016066269 was viewed at: 2016/05/17 06:13:57 | ** &gt;&gt;&gt;&gt; by: INDIA SMITH on terminal: radio4 | SPOKE TO DRIVER AND HE IS GOING THRU HIS INSURANCE TO GET THE VEH MOVED AND IF | HE UNDERSTOOD HE WILL CALL US BACK TO LET US KNOW WHAT KIND OF ARRANGEMENTS THEY | ARE MAKING FOR HIM | ** LOI information for Event # P2016066269 was viewed at: 2016/05/17 06:20:20 | ** &gt;&gt;&gt;&gt; by: KALA SMITH on terminal: px1 | ** Event Location changed from \"69 WY 220 NATR\" to \"67 WY 220 NATR\" at: | **LOI search  of CAD table completed at 2016/05/17 06:45:15 | 2016/05/17 06:45:15 | ** &gt;&gt;&gt;&gt; by: LARRY SMITH on terminal: radio4 | ** Cross Referenced to Event # P2016066278 at: 2016/05/17 06:45:38 | ** &gt;&gt;&gt;&gt; by: MIKE SMITH on terminal: radio4 | TK IS WELL OFF THE ROAD, ANTELOPE IS DEAD AND OFF THE ROADWAY | WILL TRY AND CONTACT THE DRIVER LATER TO GET MORE INFO | Preempt | Event P2016066269 Dispatch Assigned | **VEH search of CAD table completed at 2016/05/17 08:14:54 | **VEH search of CAD table completed at 2016/05/17 08:36:43 | **LOI search  of CAD table completed at 2016/05/17 14:05:46 | BE -76 TO SINCLAIR REFINERY IN EVANSVILLE TO MEET WITH THE DRIVER OF THIS | 10-50/10-4 - Alarm Timer Extended: 0 | Preempt | Event P2016066269 Dispatch Assigned | P144 ADV HAVE TO PUT THIS ON HOLD AND BE -76 TO AN AOA WITH P174 | Preempt | P144 ADV HE MADE CONTACT WITH THE DRIVER AND WILL TRY TO CONTACT HIM AGAIN | TOMORROW MORNING/10-4 WANT ME TO PUT THIS ON HOLD TILL THEN?/AFFIRM/10-4 | ** Event held for 897 minutes"';
  // before = '  "Comments": "| VEH VS DEER | DRIVERS NAME FRANK DOE 111-1111111 | RP IS JANE DOE 307-123-1234 | VEH IS BRO 2016 TOYOTA TUNDRA WY/6-6T18039 | ** &gt;&gt;&gt;&gt; by: SUNDAY CATHCART on terminal: radio4 | ** &gt;&gt;&gt;&gt; by: CALA L. HERDER on terminal: px1 | ** &gt;&gt;&gt;&gt; by: SUNDAY CATHCART on terminal: radio4 | ** &gt;&gt;&gt;&gt; by: SUNDAY CATHCART on terminal: radio4 | ** &gt;&gt;&gt;&gt; by: CALA L. HERDER on terminal: px1 | ** &gt;&gt;&gt;&gt; by: CALA L. HERDER on terminal: px1 | ** &gt;&gt;&gt;&gt; by: SUNDAY CATHCART on terminal: radio4 | ** &gt;&gt;&gt;&gt; by: SUNDAY CATHCART on terminal: radio4 | ** &gt;&gt;&gt;&gt; by: SUNDAY CATHCART on terminal: radio4 | ** &gt;&gt;&gt;&gt; by: CALA L. HERDER on terminal: px1 | ** &gt;&gt;&gt;&gt; by: SUNDAY CATHCART on terminal: radio4 | ** &gt;&gt;&gt;&gt; by: SUNDAY CATHCART on terminal: radio4"';
  // before = ' ** &gt;&gt;&gt;&gt; by: SUNDAY CATHCART on terminal: radio4 ';
  // before = ' ** &gt;&gt;&gt;&gt; by: CALA L. HERDER on terminal: px1 ';
  // before = ' ** &gt;&gt;&gt;&gt; by: SUNDAY CATHCART on terminal: radio4 | ** &gt;&gt;&gt;&gt; by: CALA L. HERDER on terminal: px1 ';
  // before = '123-456-7890 foo (123) 456-7890 bar1234567890 biz P0987654321 baz 760-2484 boz 7602484 buz 123-4567';
  console.log('before:', before);
  console.log(' after:', fixit(before));
}

function fixit(before) {
  // ** &gt;&gt;&gt;&gt; by: SUNDAY CATHCART on terminal: radio4
  var after = before.replace(/&gt; by: [A-Za-z\-\.\s]+ on /g, '&gt; by: XXXX XXX on ');
  after = after.replace(/([^PBG]|\b)(\(?\d{3}\)?)([\s-]*)(\d{3})([\s-]*)(\d{4})/g, '$1$2$3000$50000');
  after = after.replace(/\b(\d{3})([\s-]*)(\d{4})\b/g, '000$20000');
  after = after.replace(/\bNAME [A-Z]+ [A-Z]+\b/, 'NAME XXXX XXXXX')
  return after;
}
