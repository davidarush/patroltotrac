#!/bin/bash
TODAY=`date +%Y-%m-%d`
echo TODAY=$TODAY
DIR=/mnt/iiasp01/tasks/patrol
if [ ! -d $DIR ]; then
  echo Directory not found: $DIR
  exit 1
fi
echo 
FILENAME=$DIR/events-$TODAY.csv
echo $FILENAME
cat $FILENAME
echo $FILENAME

#1:event,2:created,3:updated,4:closed,5:street,6:milepost,7:direction,8:city,9:crossStreet1,10:crossStreet2,11:agency,12:type,13:description
#cat $FILENAME | cut -d , -f 1,13
cat $FILENAME | cut -d , -f 12,13 | sort | uniq
