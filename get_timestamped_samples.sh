#!/bin/bash
SRC=/mnt/iiasp01/tasks/patrol
if [ ! -d $SRC ]; then
  echo Source directory not found: $SRC
  exit 1
fi
cp $SRC/sample_201*.json .
