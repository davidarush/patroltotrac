#!/bin/bash
set -x
#CP=build/classes
#CP=$CP:~/w/wydgets/build/wydgets.jar
#CP=$CP:~/w/WyData/build/wydata.jar
#CP=$CP:~/java/lib/log4j-1.2.17.jar
#CP=$CP:~/java/lib/commons-codec-1.9/commons-codec-1.9.jar
#CP=$CP:~/java/lib/oracle/ojdbc6.jar
#CP=$CP:~/java/lib/commons-net-3.3/commons-net-3.3.jar
# add current dir so it can find .properties files.
CP=.:build/classes:$CLASSPATH
#:~/java/lib/XOM/xom-1.2.10.jar:~/w/wydgets/build/wydgets.jar:~/java/lib/log4j-1.2.17.jar
java -cp $CP wyoroad.patrolToTrac.PatrolToTrac $*
