#!/bin/bash
SRC=build/patrolToTrac.jar
#DEST=/mnt/iiasp01/tasks/patrol
DEST=/Volumes/iiasp01/tasks/patrol

ant clean
if [ $? != 0 ]; then
  echo Build failed.
  exit 1
fi

ant -Dconfig=production.properties jar
if [ $? != 0 ]; then
  echo Build failed.
  exit 1
fi

if [ -f $SRC ]; then
    echo Exists: $SRC
  else
    echo Does not exist: $SRC
    exit 1
fi

if [ -d $DEST ]; then
    echo Exists: $DEST
  else
    echo Does not exist: $DEST
    exit 1
fi

cp -v $SRC $DEST/.
cp -v patrolToTrac.properties $DEST/.
#cp -v prod_iiasp01.bat $DEST/.
cp -v PatrolToTrac.bat $DEST/.
#cp -v setup_sched_task.bat $DEST/.
#cp -v delete_sched_task.bat $DEST/.
cp -v log4j.properties $DEST/.

if true; then
  # These are really on deepsouth at d:\java\lib, and referenced in d:\w\set_paths.bat
  rsync --progress ~/w/wydgets/build/wydgets.jar $DEST/.
  rsync --progress ~/w/scrambler/build/scrambler.jar $DEST/.
  rsync --progress ~/w/WyData/build/wydata.jar $DEST/.
  rsync --progress ~/w/JSON-java/build/json.jar $DEST/.
fi

if false; then
  cp -v ~/java/lib/log4j-1.2.17.jar $DEST/.
  cp -v ~/java/lib/commons-codec-1.9/commons-codec-1.9.jar $DEST/.
  cp -v ~/java/lib/oracle/ojdbc6.jar $DEST/.
  cp -v ~/java/lib/commons-net-3.3/commons-net-3.3.jar $DEST/.
  cp -v ~/java/lib/commons-io-2.4/commons-io-2.4.jar $DEST/.
  rsync --progress ~/java/lib/httpcomponents-client-4.5.1/lib/httpclient-4.5.1.jar $DEST/.
  rsync --progress ~/java/lib/httpcomponents-core-4.4.1/lib/httpcore-4.4.1.jar $DEST/.
  rsync --progress ~/java/lib/commons-logging-1.2/commons-logging-1.2.jar $DEST/.
fi
