#!/bin/bash
if [ ! -f $1 ]; then
  echo Not found: $1
  exit 1
fi
cut -d , -f 12,13 $1 | sort | uniq
