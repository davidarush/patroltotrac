#!/bin/bash
if true; then
  PROPS=development.properties
else
   PROPS=production.properties
fi
echo PROPS = $PROPS
ant clean
ant -Dconfig=$PROPS jar compile.tests
if [ $? != 0 ]; then
  echo Bah.
  exit 1;
fi
echo PROPS = $PROPS

# add current dir so it can find .properties files.
CP=.:build/test_classes:$CLASSPATH:~/java/lib/commons-io-2.4/commons-io-2.4.jar
#:~/java/lib/XOM/xom-1.2.10.jar:~/w/wydgets/build/wydgets.jar:~/java/lib/log4j-1.2.17.jar
java -cp $CP CheckTracTasks $*
echo PROPS = $PROPS
