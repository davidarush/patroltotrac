#!/bin/bash
MNT=/mnt/iiasp01
TARGET_DIR=/mnt/iiasp01/tasks/patrol
if [ ! -d $TARGET_DIR ]; then
  echo Not found: $TARGET_DIR
  exit 1
fi
#ls -ltr $TARGET_DIR

#sample_20180924_044759.json
#events-2018-09-22.csv

AGE_DAYS=60
echo AGE_DAYS=$AGE_DAYS

echo Found `find $TARGET_DIR -mtime +$AGE_DAYS -type f -depth 1 -name 'sample_*_*.json' -exec echo Old file: {} ';' | wc -l` old sample JSON files to delete...
find $TARGET_DIR -mtime +$AGE_DAYS -type f -depth 1 -name 'sample_*_*.json' -exec rm -v {} ';'

echo Found `find $TARGET_DIR -mtime +$AGE_DAYS -type f -depth 1 -name 'events-????-??-??.json' -exec echo Old file: {} ';' | wc -l` old event CSV files to delete...
find $TARGET_DIR -mtime +$AGE_DAYS -type f -depth 1 -name 'events-????-??-??.json' -exec rm -v {} ';'

