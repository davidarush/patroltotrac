#!/bin/bash
DIR=/mnt/iiasp01/tasks/patrol
if [ ! -d $DIR ]; then
  echo Not found: $DIR
  exit 1
fi
#rsync --progress $DIR/patrolToTrac.1.log /tmp/.
#grep isInteresting /tmp/patrolToTrac.1.log
rsync --progress $DIR/patrolToTrac.log /tmp/.
grep isInteresting /tmp/patrolToTrac.log
