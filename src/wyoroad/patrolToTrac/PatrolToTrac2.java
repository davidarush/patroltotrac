package wyoroad.patrolToTrac;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import wyoroad.wydgets.DarUtils;
import wyoroad.wydgets.Properties;
import wyoroad.wydata.PatrolEvent;
import wyoroad.wydata.TracTask;
import java.util.List;
import java.util.ArrayList;
import wyoroad.wydgets.WebService;
import java.net.URL;
import java.io.File;
import java.io.FileWriter;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONTokener;
import org.json.JSONException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.http.StatusLine;

import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;

import org.apache.commons.io.IOUtils;

import java.security.cert.X509Certificate;
import java.security.cert.CertificateException;

import java.net.URLDecoder;
import java.net.URI;
import java.util.Set;
import java.util.HashSet;
import wyoroad.wydgets.FileStuff;
import wyoroad.wydgets.DatabaseReader;
import wyoroad.wydgets.DarUtils;
import wyoroad.scrambler.Scrambler;
import wyoroad.scrambler.CipherText;
import wyoroad.scrambler.PlainText;
import org.apache.log4j.Logger;
import wyoroad.wydgets.sql.SqlSelect;
import wyoroad.wydgets.sql.SqlTableName;
import wyoroad.wydgets.sql.SqlSequenceName;
import wyoroad.wydgets.KeyGenerator;
import java.util.Map;
import java.util.HashMap;
import wyoroad.wydgets.Whenstamp;
import java.net.URLEncoder;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.StringReader;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.ContentType;
import wyoroad.wydata.WatchDogHeartBeat;
import java.sql.ResultSet;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import wyoroad.wydgets.trac.TracHighlighter;


public class PatrolToTrac2 {
  private static final Logger sLogger = Logger.getLogger(PatrolToTrac2.class);
  private static final boolean WRITE_SAMPLE_JSON_FILE = true;
  private static final boolean DELETE_TIMESTAMPED_SAMPLE_JSON_FILE = true;
  private static final boolean REPORT_TO_TRAC = true;
  private static final boolean INCLUDE_COMMENTS_IN_TRAC_TASK = true;
  private static final int DEFAULT_TRAC_PRIORITY = TracTask.PRIORITY_HIGH;
  private static final String ENCODING = "UTF-8";
  public static final boolean OBSCURE_DISPATCHER_NAMES = false;
  public static final boolean WRITE_EVENTS_TO_CSV = true;
  private static Scrambler s_scrambler = null;
  private static final byte[] WHP_COMMENTS_SCRAMBLER_KEY = {-96,-57,118,38,-47,14,49,-89,-108,-126,113,-95,118,56,115,15};
  private static Map<String,Integer> cityToDistrict = null;



  public static void main(String[] args) {
    // Create a filename, which the sample we're about to grab will be saved as.
    // If all goes well, it will be deleted when the program is done.
    // If there's something wrong with the file, an exception might be thrown and the file will not be deleted.
    Whenstamp now = new Whenstamp();
    File sampleFile = new File("sample_" + now.getYYYYMMDD_HHMMSS() + ".json");
    try {
      sLogger.info("\n\nmain() starting");
      // Look at the first command line argument.  Is it a URL?  If not, assume it's a file.
      PatrolToTrac2 t = null;
      if (args.length > 0) {
        t = new PatrolToTrac2(new File(args[0]));
      } else {
        t = new PatrolToTrac2();
      }
      t.go(sampleFile);
      if (DELETE_TIMESTAMPED_SAMPLE_JSON_FILE) {
        sampleFile.delete();
      }
    } catch (Exception ex) {
      sLogger.error("Caught in main: ", ex);
      ex.printStackTrace(System.err);
      System.exit(1);
    }
    System.exit(0);
  }


  protected URI uri = null;
  protected File file = null;
  protected SqlTableName tableName = null;
  protected SqlSequenceName sequenceName = null;
  protected SqlSelect select = null;
  protected String tracUrl = null;
  protected List<String> messages = null;
  protected Scrambler scrambler = null;

  protected String dbUrl = null;
  protected String dbUsername = null;
  protected PlainText dbPassword = null;
  protected Properties props = null;
  protected SqlTableName watchDogHeartBeatTableName = null;
  protected String watchDogHeartBeatName = null;
  protected int watchDogHeartBeatMaxAgeSecs = 0;
  protected SqlSelect cityToDistrictSelect = null;
  protected URI gbffeCNtoDistrictUri = null;
  private Set<String> unrecognizedCities = null;
  private static TracHighlighter s_tracHighlighter = null;


  public PatrolToTrac2() throws Exception {
    // Will pull URI from properties file later.
    this.uri = null;
    this.file = null;
    build();
  }

  public PatrolToTrac2(File file) throws Exception {
    // Use the given file as input, rather than URI from the properties file.
    this.file = file;
    this.uri = null;
    build();
  }

  protected void build() throws Exception {
    this.props = new Properties(new File("patrolToTrac.properties"));
    this.dbUrl = this.props.get("db_url");
    this.dbUsername = this.props.get("db_username");
    this.dbPassword = new PlainText(Scrambler.Decrypt(new CipherText(this.props.get("db_password"))));
    this.select = new SqlSelect(this.props.get("patrol_events_select"));
    sLogger.debug("this.select = " + this.select);
    this.tableName = new SqlTableName(this.props.get("patrol_events_table_name"));
    sLogger.debug("this.tableName = " + this.tableName);
    this.sequenceName = new SqlSequenceName(this.props.get("patrol_events_sequence_name"));
    sLogger.debug("this.sequenceName = " + this.sequenceName);
    this.tracUrl = this.props.get("trac_newtask_url");
    sLogger.debug("this.tracUrl = " + this.tracUrl);

    this.watchDogHeartBeatTableName  = new SqlTableName(this.props.get("watch_dog_heart_beat_table_name"));
    this.watchDogHeartBeatName       = this.props.get("watch_dog_heart_beat_name");
    this.watchDogHeartBeatMaxAgeSecs = this.props.getInt("watch_dog_heart_beat_max_age_secs", 310);

    this.cityToDistrictSelect  = new SqlSelect(this.props.get("patrol_city_district_select"));

    this.scrambler = new Scrambler(WHP_COMMENTS_SCRAMBLER_KEY);
    this.gbffeCNtoDistrictUri = new URI(this.props.get("gbffe_cn_to_district_uri"));

    s_tracHighlighter = new TracHighlighter()
                           .dbUrl(this.props.get("trac_highlight_db_url"))
                           .dbUsername(this.props.get("trac_highlight_db_username"))
                           .dbPassword(new CipherText(this.props.get("trac_highlight_db_password")))
                           .select(new SqlSelect(this.props.get("trac_highlight_select")))
                           .bake();
  }


  private void populateCityToDistrict(DatabaseReader reader) throws Exception {
    if (reader == null) throw new NullPointerException("reader is null");
    if (!reader.isOpen()) throw new Exception("reader is not open");
    cityToDistrict = new HashMap<String,Integer>();
    this.unrecognizedCities = new HashSet<String>();
    // Read from the DB
    ResultSet rs = null;
    try {
      rs = reader.execute(cityToDistrictSelect);
      while (rs.next()) {
        String abbrev = rs.getString("ABBREV");
        int district = rs.getInt("DISTRICT");
        String city = rs.getString("CITY");
        sLogger.debug(String.format("%-7s %1d %s", abbrev, district, city));
        cityToDistrict.put(abbrev, district);
      }
    } finally {
      DarUtils.closeStmtToo(rs);
    }
  }


  private int getDistrict(PatrolEvent event) throws Exception {
    int result = 0;
    sLogger.debug("Looking up district for event: " + event);
    // First try looking up district by city name and lookup table
    String city = event.getCity();
    if (city == null) {
      result = 0;
    } else {
      result = getDistrict(city);
    }
    // If the city name didn't find anything, then try looking it up by common route name and reference marker (milepost).
    if (result == 0) {
      try {
        result = getDistrictByNameAndRefMarker(event);
      } catch (Exception ex) {
        sLogger.warn("Caught exception trying to get district by common name and reference marker: ", ex);
        sLogger.warn("event = " + event);
        result = 0;
      }
    }
    return result;
  }


  private int getDistrict(String city) {
    int district = 0;
    if (cityToDistrict.containsKey(city)) {
      district = cityToDistrict.get(city);
      sLogger.debug("City " + DarUtils.qs(city) + " is in district " + district);
    } else {
      sLogger.info("City-to-district not found for " + DarUtils.qs(city));
      this.unrecognizedCities.add(city);
    }
    return district;
  }


  private int getDistrictByNameAndRefMarker(PatrolEvent event) throws Exception {
    int result = 0;
    String commonName = event.getStreet();
    if (looksLikeHighway(commonName)) {
      sLogger.debug("Looks like a highway name: " + commonName);
      String tmp = event.getRefMarker();
      double refMarker = DarUtils.parseDouble(tmp, Double.NaN);
      result = getDistrict(commonName, refMarker);
    } else {
      sLogger.debug("Rejecting highway name: " + DarUtils.qs(commonName));
    }
    return result;
  }

  protected static final Pattern HIGHWAY_NAME_PAT = Pattern.compile("^(I|US|WY|WYO|CO|VA|GA)[-\\s]*(\\d+)\\s*(.*)$");

  public static boolean looksLikeHighway(String name) {
    Matcher mat = HIGHWAY_NAME_PAT.matcher(name);
    return mat.find();
  }


  public int getDistrict(String commonName, double refMarker) throws Exception {
    int district = 0;
    if (commonName != null && !Double.isNaN(refMarker)) {
      JSONObject jo = getJSONObject(this.gbffeCNtoDistrictUri, commonName, refMarker);
      if (jo != null) {
        try {
          district = jo.getInt("district");
          // Look at the results array.  If more than one district number is returned, then don't return any district number because it's ambiguous (return 0).
          JSONArray results = jo.getJSONArray("results");
          int size = results.length();
          boolean mismatch = false;
          for(int j = 0; j < size-1; j++) {
            for(int k = j+1; k < size; k++) {
              JSONObject jj = results.getJSONObject(j);
              JSONObject kk = results.getJSONObject(k);
              int jDist = jj.getInt("district");
              int kDist = kk.getInt("district");
              sLogger.debug("j=" + j + ", jDist=" + jDist + ", k=" + k + ", kDist=" + kDist);
              if (jDist != kDist) {
                mismatch = true;
              }
            }
          }
          if (mismatch) {
            sLogger.debug("Found a mis-match.  Returning 0.");
            district = 0;
          } else {
            sLogger.debug("No mismatch.  Good district.  Returning " + district);
          }
        } catch (Exception ex) {
          sLogger.warn("Trying to get int district from JSON: ", ex);
          sLogger.warn("jo = " + jo);
          district = 0;
        }
      }
    }
    sLogger.debug("returning district " + district);
    return district;
  }

  public URI getGbffeCNtoDistrictUri() {
    return this.gbffeCNtoDistrictUri;
  }

  public JSONObject getJSONObject(URI uri, String commonName, double refMarker) throws Exception {
    JSONObject jo = null;
    List<NameValuePair> params = new ArrayList<NameValuePair>();
    params.add(new BasicNameValuePair("commonName", commonName));
    params.add(new BasicNameValuePair("refMarker", Double.toString(refMarker)));
    return getJSONObject(uri, params);
  }


  public JSONObject getJSONObject(URI uri, List<NameValuePair> params) throws Exception {
    String str = getString(uri, params);
    // Parse this string into a JSON object
    JSONObject jo = new JSONObject(new JSONTokener(str));
    return jo;
  }


  public String getString(URI uri, List<NameValuePair> params) throws Exception {
    final boolean USE_PROXY = false;
    // Example taken from Apache Http Client (hc.apache.org) QuickTest.java
    CloseableHttpClient httpclient = HttpClients.createDefault();
    RequestConfig config = null;
    HttpHost target = null;
    HttpPost httpPost = new HttpPost(uri);
    httpPost.setEntity(new UrlEncodedFormEntity(params, ENCODING));
    if (USE_PROXY) {
      HttpHost proxy = null;
      target = new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme());
      proxy = new HttpHost("127.0.0.1", 8888, "http");
      uri = new URI(uri.getPath());
      config = RequestConfig.custom().setProxy(proxy).build();
    }
    if (USE_PROXY) {
      httpPost.setConfig(config);
    }
    String responseBody = null;
    try {
      HttpResponse response = null;
      if (USE_PROXY) {
        response = httpclient.execute(target, httpPost);
      } else {
        response = httpclient.execute(httpPost);
      }
      StatusLine statusLine = response.getStatusLine();
      sLogger.debug("statusCode = " + statusLine.getStatusCode());
      if (statusLine.getStatusCode() != 200) {
        sLogger.warn("For URI " + uri);
        throw new Exception(statusLine.toString());
      }
      HttpEntity entity = response.getEntity();
      responseBody = EntityUtils.toString(entity);
      // System.out.println("responseBody = " + responseBody);
      EntityUtils.consume(entity);
    } finally {
      httpPost.releaseConnection();
    }
    return responseBody;
  }


  /**
    This is where the real work starts.
    @param sampleFile The file to which a copy of the raw data, as received from WHP will be written (to be deleted later if everything goes well).
    @throws Exception if something goes awry
   */
  public void go(File sampleFile) throws Exception {
    String rawData = getRawData(this.props, sampleFile);  // get raw string from URI or file
    if (rawData == null) {
      sLogger.warn("rawData is null");
      sLogger.error("this.uri=" + this.uri + ", this.file=" + this.file + ", but rawData is null");
      // System.err.println("rawData is null");
    } else if (rawData.indexOf("[{\"Records\",0}]") > 0) {
      sLogger.debug("rawData is empty (Records,0)");
    } else {
      JSONObject jroot = parseJSON(rawData);  // parse raw data string into JSON
      // jroot is not null, should hold JSONArray named 'Events'
      // Get "new" events from the web service (or file)
      List<PatrolEvent> newEvents = getEvents(jroot);
      newEvents = removeUninterestingEvents(newEvents);
      logEvents("newEvents", newEvents);
      if (newEvents == null) {
        sLogger.debug("newEvents is null - nothing new going on.");
      } else if (newEvents.size() == 0) {
        sLogger.debug("No newEvents to process.");
      } else {
        sLogger.debug("Found " + newEvents.size() + " new events to process");
        processNewEvents(newEvents, this.props);
      }
    }
    if (sLogger.isDebugEnabled()) {
      if (this.messages == null || this.messages.size() == 0) {
        sLogger.debug("No messages.");
      } else {
        int j = 0;
        for(String msg: this.messages) {
          sLogger.debug("messages[" + j++ + "] = " + msg);
        }
      }
    }
    logUnrecognizedCities();
    updateHeartbeat();
  }


  private void logUnrecognizedCities() {
    if (this.unrecognizedCities != null && this.unrecognizedCities.size() > 0) {
      StringBuilder sb = new StringBuilder();
      sb.append("unrecognizedCities = ");
      boolean first = true;
      for(String city: this.unrecognizedCities) {
        if (first) {
          first = false;
        } else {
          sb.append(", ");
        }
        sb.append(city);
      }
      sLogger.info(sb.toString());
    }
  }


  private String getRawData(Properties props, File sampleFile) throws Exception {
    String rawData = null;
    if (this.file != null) {
      sLogger.debug("Using file: " + this.file);
      rawData = getRawData(this.file);
    } else {
      URI uri = new URI(props.get("patrol_events_url"));
      sLogger.debug("Using URI: " + uri);
      rawData = getRawData(uri);
    }
    if (rawData == null) {
      sLogger.warn("rawData is null");
    } else {
      sLogger.debug("rawData is " + rawData.length() + " chars long.");
      // save it to a file
      FileStuff.writeTextFile(sampleFile, rawData);
    }
    return rawData;
  }


  private void processNewEvents(List<PatrolEvent> newEvents, Properties props) throws Exception {
    // Grab "old" events from the DB
    DatabaseReader reader = null;
    try {
      reader = getReader(props);
      populateCityToDistrict(reader);
      List<PatrolEvent> oldEvents = getOldEvents(reader, this.select);
      sLogger.info("Read " + oldEvents.size() + " events from database: " + this.select);
      logEvents("oldEvents", oldEvents);
      process(oldEvents, newEvents, reader);
    } finally {
      DarUtils.close(reader);
    }
  }


  private DatabaseReader getReader(Properties props) throws Exception {
    DatabaseReader reader = null;
    //String dbUrl = props.get("db_url");
    sLogger.debug("dbUrl = " + this.dbUrl);

    //String dbUsername = props.get("db_username");
    sLogger.debug("dbUsername = " + this.dbUsername);

    //PlainText dbPassword = new PlainText(Scrambler.Decrypt(new CipherText(props.get("db_password"))));
    // sLogger.debug("dbPassword = " + dbPassword);

    reader = new DatabaseReader(this.dbUrl, this.dbUsername, this.dbPassword);
    reader.setTracer("PatrolToTrac2-getEvents");
    reader.open();
    return reader;
  }


  public static List<PatrolEvent> getEvents(JSONObject jroot) throws JSONException {
    JSONArray jsonEvents = jroot.getJSONArray("Events");
    List<PatrolEvent> patrolEvents = new ArrayList<PatrolEvent>();
    String tmp = null;
    for(int j = 0; j < jsonEvents.length(); j++) {
      JSONObject jsonEvent = jsonEvents.getJSONObject(j);
      //if (Math.random() < 0.5) {
      //  jsonEvent.put("County", "Rush");
      //}
      String eventId     = jsonEvent.getString("Event");
      tmp     = jsonEvent.optString("Created");
      Whenstamp created = null;
      if (tmp != null) {
        created = new Whenstamp(tmp);
      }
      tmp     = jsonEvent.optString("Updated");
      Whenstamp updated = null;
      if (!DarUtils.isEmpty(tmp)) {
        updated = new Whenstamp(tmp);
      }

      PatrolEvent patrolEvent = new PatrolEvent(eventId, created, updated);
      sLogger.debug("jsonEvent = " + jsonEvent.toString(2));
      patrolEvent.agency(get(jsonEvent, "Agency"));
      patrolEvent.type(get(jsonEvent, "Type"));
      patrolEvent.description(get(jsonEvent, "Description"));
      patrolEvent.county(get(jsonEvent, "County"));
      patrolEvent.direction(get(jsonEvent, "Dir"));
      patrolEvent.street(get(jsonEvent, "Street"));
      patrolEvent.refMarker(get(jsonEvent, "Reference Marker"));
      patrolEvent.city(get(jsonEvent, "City"));
      // getDistrict(patrolEvent);
      patrolEvent.crossStreet1(get(jsonEvent, "Cross Street 1"));
      patrolEvent.crossStreet2(get(jsonEvent, "Cross Street 2"));
      patrolEvent.comments(cleanComments(get(jsonEvent, "Comments")));
      tmp     = jsonEvent.optString("Closed");
      Whenstamp closed = null;
      if (!DarUtils.isEmpty(tmp)) {
        closed = new Whenstamp(tmp);
      }
      patrolEvent.closed(closed);

      sLogger.debug("Adding to patrolEvents [" + j + "]: " + patrolEvent);
      sLogger.trace("comments:\n" + patrolEvent.getComments());
      sLogger.debug("\n\n");
      highlight(patrolEvent);
      patrolEvents.add(patrolEvent);
    }
    return patrolEvents;
  }

  public static String get(JSONObject jo, String name) {
    String value = null;
    if (jo.has(name)) {
      if (!(jo.isNull(name))) {
        value = jo.getString(name);
      }
    }
    return value;
  }


  /**
    Clean up the JSON-ish WHP output to something parseable.  Will either return
    a valid non-null JSONObject or throw an exception.
    @param before Raw JSON-ish WHP data, which has some HTML-ness wrapped around it.
   */
  public static JSONObject parseJSON(String before) throws Exception {
    // Clean up the JSON
    // Skip all the HTML-ness that shouldn't be part of a JSON object
    String after = deHTML(before);
    JSONObject jo = null;
    try {
      jo = new JSONObject(new JSONTokener(after));
    } catch (JSONException ex) {
      sLogger.warn("Error parsing JSON, before = " + before);
      sLogger.warn("after = " + after);
      throw ex;
    }
    // System.out.println("\n\n\njo = " + jo.toString(2));
    if (jo == null) {
      throw new NullPointerException("jo is null, but should not be at this point.");
    }
    return jo;
  }


  public static String deHTML(String before) {
    String after = before;
    // strip everything up to the first curly bracket
    int n = before.indexOf("{");
    if (n > 0) {
      after = before.substring(n);
    }
    // There is some ugly HTML markup interspersed into the JSON data, presumably to make it "look right" in a browser window.
    // But it's illegal JSON and will not parse, so we have to clean it up.
    after = after.replaceAll("</pre>\\s*</body>\\s*</html>","");
    after = after.replaceAll("<br>\\s*\\[", "[");
    after = after.replaceAll("<br>\\s*}", "}");
    after = after.replaceAll("\\[<br> \\{<br>", "[{");
    after = after.replaceAll("\", <br> \"", "\", \"");
    after = after.replaceAll("<pre>", ""); // service results with zero records have an extra <pre> in there.  I have no idea why.
    // System.out.println("\n\n\nbefore = " + before);
    // System.out.println("\n\n\nafter = " + after);
    after = after.replace("\"Records\",0", "\"Records\":0"); // just guessing that this is what was intended
    return after;
  }


  private static void logEvents(String prefix, List<PatrolEvent> list) {
    if (sLogger.isDebugEnabled()) {
      if (list == null) {
        sLogger.warn(prefix + ": list is null");
      } else {
        int j = 0;
        for(PatrolEvent pe: list) {
          sLogger.debug(prefix + "[" + j + "] = " + pe.getEvent() + " " + pe.getCreated() + " " + pe.getUpdated());
          j++;
        }
      }
    }
  }


  /**
    Get the raw String data from the given URI, assumed to be the WHP web service, exactly as the web service provides it.
   */
  public static String getRawData(URI uri) throws Exception {
    String responseBody = null;
    // Properties props = new Properties(new File("patrolToTrac.properties"));
    // URI uri = new URI(uri);
    sLogger.info("Getting data from URI " + uri);
    SSLContextBuilder builder = new SSLContextBuilder();
    builder.loadTrustMaterial(null, new WayTooLenientTrustStrategy());
    SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(builder.build(), SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
    CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
    HttpGet httpGet = new HttpGet(uri);
    CloseableHttpResponse response = httpclient.execute(httpGet);
    try {
      StatusLine statusLine = response.getStatusLine();
      // System.out.println("Status line: " + statusLine);
      HttpEntity entity = response.getEntity();
      if (statusLine.getStatusCode() == 200) {
        responseBody = EntityUtils.toString(entity);
        // System.out.println("responseBody = " + responseBody);
      } else {
        sLogger.error("Bad HTTP status result of: " + statusLine);
        throw new Exception("HTTP status not 200: " + statusLine);
      }
      EntityUtils.consume(entity);
    } finally {
      response.close();
    }
    if (WRITE_SAMPLE_JSON_FILE) {
      FileStuff.writeTextFile(new File("sample.json"), responseBody);
    }
    return responseBody;
  }


  /**
    Get the raw String data from a file, presumed to be in the same (flawed) format as the WHP web service provides it.
   */
  public static String getRawData(File file) throws Exception {
    return FileStuff.fileToString(file);
  }


  /**
    For each newEvent, look to see if it exists in oldEvents, and if it does see if it needs updating.
    If it's new or needs updating, it creates a new TracTask for it.
   */
  public void process(List<PatrolEvent> oldEvents, List<PatrolEvent> newEvents, DatabaseReader reader) throws Exception {
    if (oldEvents == null) throw new NullPointerException("oldEvents is null");
    if (newEvents == null) throw new NullPointerException("newEvents is null");
    if (reader == null) throw new NullPointerException("reader is null");
    if (!(reader.isOpen())) throw new Exception("reader is not open");
    int reportedCount = 0;
    int eventCount = 0;
    for(PatrolEvent newEvent: newEvents) {
      sLogger.debug("Processing newEvent " + newEvent.getEvent());
      if (process(oldEvents, newEvent, reader)) {
        reportedCount++;
        sLogger.debug("did report event " + newEvent.getEvent());
      } else {
        sLogger.debug("did not report event " + newEvent.getEvent());
      }
      eventCount++;
      sLogger.debug("eventCount = " + eventCount + ", reportedCount = " + reportedCount);
    }
    sLogger.info("Processed " + eventCount + " events, reported " + reportedCount + " events.");
  }


  /**
    Checks newEvent against oldEvents to see if it's new (not in oldEvents) or updated (updated timestamp newer than matching event in oldEvents).
    @return True if newEvent is reportable (new or updated event)
   */
  public boolean process(List<PatrolEvent> oldEvents, PatrolEvent newEvent, DatabaseReader reader) throws Exception {
    if (oldEvents == null) throw new NullPointerException("oldEvents is null");
    if (newEvent == null) throw new NullPointerException("newEvent is null");
    boolean reportable = false;
    PatrolEvent latestOldEvent = getLatestUpdated(oldEvents, newEvent.getEvent());
    if (latestOldEvent != null) {
      sLogger.debug("  Found event " + newEvent.getEvent() + " in oldEvents");
      // check to see if the updated timestamp in the newEvent is later than the latestOldEvent
      if (newEvent.getUpdated() == null) {
        // if updated is empty but found an oldEvent, then ignore it because it's just an event that has never been updated.
        this.addMessage("Ignoring event " + newEvent.getEvent() + " updated " + newEvent.getUpdated());
      } else if (latestOldEvent.getUpdated() == null) {
        // newEvent updated is NOT null, but latestOldEvent is null, so it must have been updated
        sLogger.debug("  Event " + newEvent.getEvent() + " has been updated.  This should be reported.");
        this.addMessage("Updating event " + newEvent.getEvent() + " updated " + newEvent.getUpdated() + ", previously not updated");
        reportable = true;
      } else if (latestOldEvent.getUpdated() == null || newEvent.getUpdated().isAfter(latestOldEvent.getUpdated())) {
        sLogger.debug("  Event " + newEvent.getEvent() + " has been updated.  This should be reported.");
        this.addMessage("Updating event " + newEvent.getEvent() + " updated " + newEvent.getUpdated());
        reportable = true;
      } else {
        sLogger.debug("  Event " + newEvent.getEvent() + " has NOT been updated.  Nothing to do.");
      }
    } else {
      sLogger.debug("  Event " + newEvent.getEvent() + " not found in oldEvents.  This should be reported.");
      this.addMessage("Adding event " + newEvent.getEvent() + " updated " + newEvent.getUpdated());
      reportable = true;
    }
    if (!reportable) {
      this.addMessage("Ignoring event " + newEvent.getEvent() + " updated " + newEvent.getUpdated());
    }
    sLogger.debug("reportable = " + reportable);
    if (reportable) {
      report(reader, newEvent);
    }
    return reportable;
  }


  /**
    Send this event to TRAC and save the sanitized event (all but event ID,
    created, and updated removed) to the DB.
    */
  public void report(DatabaseReader reader, PatrolEvent event) throws Exception {
    if (event == null) throw new NullPointerException("event is null");
    sLogger.debug("Need to report: " + event.getEvent());
    sendToTrac(reader, event);
    writeToCSV(event);
    if (false) {
      // old way
      event.sanitize();  // remove all info except event ID, created, and updated dates.
    } else {
      // new way, that leaves more info in the DB so that Paul can build an Incident.  DAR 2018 Jan 19.
      event.sanitize2();  // remove all info except event ID, created, and updated dates.
    }
    addToDb(reader, event);
  }


  private void writeToCSV(PatrolEvent event) {
    FileWriter fw = null;
    boolean append = true;
    try {
      File file = new File(getCSVFileName(event));
      boolean exists = file.exists();
      fw = new FileWriter(file, append);
      if (!exists) {
        fw.write(toCSVHeaderLine());
      }
      fw.write(toCSVLine(event));
      sLogger.info("Wrote to " + file);
    } catch (Exception ex) {
      sLogger.warn("Caught in writeToCSV: ", ex);
    } finally {
      close(fw);
    }
  }


  private String getCSVFileName(PatrolEvent event) {
    Whenstamp now = new Whenstamp();
    String fileName = "events-" + now.getYYYYdMMdDD() + ".csv";
    sLogger.debug("fileName = " + fileName);
    return fileName;
  }


  private String toCSVHeaderLine() {
    StringBuilder sb = new StringBuilder();
    sb.append("event");
    sb.append(",created");
    sb.append(",updated");
    sb.append(",closed");
    sb.append(",street");
    sb.append(",milepost");
    sb.append(",direction");
    sb.append(",city");
    sb.append(",crossStreet1");
    sb.append(",crossStreet2");
    sb.append(",agency");
    sb.append(",type");
    sb.append(",description");
    sb.append("\n");
    return sb.toString();
  }
 
  
  private String toCSVLine(PatrolEvent event) {
    StringBuilder sb = new StringBuilder();
    sb.append(event.getEvent());
    sb.append(",").append(toString(event.getCreated()));
    sb.append(",").append(toString(event.getUpdated()));
    sb.append(",").append(toString(event.getClosed()));
    sb.append(",").append(toString(event.getStreet()));
    sb.append(",").append(event.getRefMarker());
    sb.append(",").append(toString(event.getDirection()));
    sb.append(",").append(toString(event.getCity()));
    sb.append(",").append(toString(event.getCrossStreet1()));
    sb.append(",").append(toString(event.getCrossStreet2()));
    sb.append(",").append(toString(event.getAgency()));
    sb.append(",").append(toString(event.getType()));
    sb.append(",").append(toString(event.getDescription()));
    sb.append("\n");
    return sb.toString();
  }

  
  private String toString(String x) {
    String result = x;
    if (x == null) {
      result = "";
    }
    return result;
  }

  
  private String toString(Whenstamp x) {
    String result = "";
    if (x != null) {
      result = x.getISO8601();
    }
    return result;
  }


  private void close(FileWriter fw) {
    if (fw != null) {
      try {
        fw.close();
      } catch (Exception ex) {
        sLogger.warn("Caught trying to close fw: ", ex);
      }
    } else {
      sLogger.warn("fw is null");
    }
  }

  /**
    From the given list, return the member of that list with a matching event
    ID, if if the list has more than one with that event ID, return the one with
    the latest (newest) updated timestamp. Returns null if the given event ID
    not found at all.
   */
  public static PatrolEvent getLatestUpdated(List<PatrolEvent> events, String eventId) {
    if (events == null) throw new NullPointerException("events is null");
    if (eventId == null) throw new NullPointerException("events is null");
    PatrolEvent latest = null;
    sLogger.debug("Target eventId = " + eventId);
    for(PatrolEvent pe: events) {
      // sLogger.debug("  Target is " + eventId + ", considering " + pe.getEvent() + ", " + pe.getUpdated());
      if (eventId.equals(pe.getEvent())) {
        // found matching event ID.  Now look to see if the one we found is
        // newer any any previously found one.
        if (latest == null) {
          sLogger.debug("    Found first matching event: " + pe.getEvent() + ", " + pe.getUpdated());
          latest = pe;
        } else if (latest.getUpdated() == null) {
          sLogger.debug("    Already found a matching event, but the old one has no updated timestamp, so keeping the new one.");
          // If a previously-found one does not have an updated timestamp (which
          // is entirely possible), then any OTHER event (which presumably DOES
          // have an updated timestamp) must be newer.
          latest = pe;
        } else if (pe.getUpdated() == null) {
          // latest's updated is not null, but this one is.  Ignore it, cuz the one with an updated timestamp MUST be newer than one without.
        } else if (pe.getUpdated().isAfter(latest.getUpdated())) {
          sLogger.debug("    Found another matching event that is newer: " + pe.getEvent() + ", " + pe.getUpdated());
          latest = pe;
        } else {
          sLogger.debug("    Already found a match, and the old one is newer than the new one.  Doing nothing.");
        }
      } else {
        // sLogger.trace("    Does not match: " + pe.getEvent());
      }
    }
    return latest;
  }


  /** Send this event to the TRAC database via its web service. */
  public void sendToTrac(DatabaseReader reader, PatrolEvent event) throws Exception {
    // private void report(String tracUrl, String reason, PatrolEvent event) throws Exception
    // Report this new or updated event to TRAC
    String description = buildDescription(event);
    sLogger.debug("description = " + description);
    final boolean OLD_WAY = false;
    if (OLD_WAY) {
      String wholeUrl = buildUrl(event, description);
      sLogger.debug("Whole URL: " + wholeUrl);
      if (REPORT_TO_TRAC) {
        sLogger.debug("Sending message to TRAC: " + description);
        String response = WebService.getString(new URL(wholeUrl));
        sLogger.debug("Web service response: " + response);
      } else {
        sLogger.warn("Not actually reporting because REPORT_TO_TRAC = " + REPORT_TO_TRAC);
      }
    } else {
      // new way, created becuz with the introduction of the loquatious "comments" field, it's just too much to bundle into a URL
      sendToTracViaPost(event);
    }
  }


  /**
    Send this event to the TRAC database table, via a POST request (because with
    the additional comments data the tasks were getting too big for a GET
    request).
    */
  public void sendToTracViaPost(PatrolEvent event) throws Exception {
    int priority = DEFAULT_TRAC_PRIORITY;
    String description = buildDescription(event);
    String comments = buildComments(event);
    List<NameValuePair> params = buildParams(event, priority, description, comments);
    URI uri = new URI(this.tracUrl);
    sendViaPost(uri, params);
  }


  private String buildDescription(PatrolEvent event) {
    StringBuilder sb = new StringBuilder();
    // build the description string
    String type = s_tracHighlighter.highlight(event.getType());
    sLogger.debug("type = " + DarUtils.qs(type));
    sb.append("<span style=\"font-weight:bold\">WHP Event ").append(event.getEvent()).append(": ").append(type);
    if (event.getIsClosed()) {
      sb.append(" <span class=\"closed\">CLOSED</span>");
    }
    sb.append("</span>");
    String desc = s_tracHighlighter.highlight(event.getDescription());
    sb.append(", ").append(desc);
    sb.append("<br/>Agency: ").append(event.getAgency());
    if (event.getCounty() != null) {
      sb.append(", County: ").append(event.getCounty());
    } else {
      // sb.append("<br/>");
    }
    if (event.getCity() != null) {
      sb.append(", City: ").append(event.getCity());
    }
    sb.append("<br/>Street: ").append(event.getStreet());
    sb.append(", Ref Marker: ").append(event.getRefMarker());
    if (event.getDirection() != null) {
      sb.append(", Direction: ").append(event.getDirection());
    }
    if (event.getCrossStreet1() != null) {
      sb.append(", Cross Street 1: ").append(event.getCrossStreet1());
    }
    if (event.getCrossStreet2() != null) {
      sb.append(", Cross Street 2: ").append(event.getCrossStreet2());
    }
    if (event.getCreated() != null) {
      sb.append("<br/>Created: ").append(event.getCreated().toString());
    } else {
      sb.append("<br/>");
    }
    if (event.getUpdated() != null) {
      sb.append(", Updated: ").append(event.getUpdated().toString());
    }
    sLogger.debug("description length is " + sb.length());
    return sb.toString();
  }


  /**
    Pull the comments out of the event, cut 'em off if they're too long, and encrypt them.
   */
  private String buildComments(PatrolEvent event) {
    String comments = null;
    if (INCLUDE_COMMENTS_IN_TRAC_TASK && event.getComments() != null) {
      int maxCommentsLength = (int)(TracTask.getCommentsMaxLen() / 1.5);  // encryption and base64 encoding increases size by about 1.4
      comments = event.getComments();
      sLogger.debug("comments.length() = " + comments.length() + ", maxCommentsLength = " + maxCommentsLength);
      if (comments.length() > maxCommentsLength) {
        int end = comments.length();
        int start = end - maxCommentsLength;
        sLogger.debug("start = " + start + ", end = " + end);
        comments = comments.substring(start, end);
        sLogger.debug("shortened comments.length() = " + comments.length());
      }
    }
    return comments;
  }


  private List<NameValuePair> buildParams(PatrolEvent event, int priority, String description, String comments) throws Exception {
    List<NameValuePair> params = new ArrayList<NameValuePair>();
    params.add(new BasicNameValuePair("priority", Integer.toString(priority)));
    params.add(new BasicNameValuePair("description", description));
    if (comments == null) {
      params.add(new BasicNameValuePair("comments", null));
    } else {
      CipherText cipherText = new CipherText(this.scrambler.encrypt(new PlainText(comments)));
      sLogger.debug("plaintext comments = " + comments);
      sLogger.debug("ciphertext comments = " + cipherText);
      sLogger.debug("plaintext length = " + comments.length() + ", cipherText length = " + cipherText.length());
      if (cipherText.length() > TracTask.getCommentsMaxLen()) {
        sLogger.warn("Encrypted length of comments is too big for DB at event " + event.getEvent() + " updated " + event.getUpdated());
      } else {
        params.add(new BasicNameValuePair("comments", cipherText.toString()));
      }
    }
    params.add(new BasicNameValuePair("source", "WHP"));
    params.add(new BasicNameValuePair("createdBy", "WHP 2"));
    int district = getDistrict(event);
    if (district > 0 && district < 6) {
      params.add(new BasicNameValuePair("district", Integer.toString(district)));
    }
    return params;
  }


  /**
    Send the given parameters to the given URI via a POST request.
   */
  public void sendViaPost(URI uri, List<NameValuePair> params) throws Exception {
    final boolean USE_PROXY = false;
    // Example taken from Apache Http Client (hc.apache.org) QuickTest.java
    CloseableHttpClient httpclient = HttpClients.createDefault();
    RequestConfig config = null;
    HttpHost target = null;
    HttpPost httpPost = new HttpPost(uri);
    httpPost.setEntity(new UrlEncodedFormEntity(params, ENCODING));
    if (USE_PROXY) {
      HttpHost proxy = null;
      target = new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme());
      proxy = new HttpHost("127.0.0.1", 8888, "http");
      uri = new URI(uri.getPath());
      config = RequestConfig.custom().setProxy(proxy).build();
    }
    if (USE_PROXY) {
      httpPost.setConfig(config);
    }
    String responseBody = null;
    try {
      HttpResponse response = null;
      if (USE_PROXY) {
        response = httpclient.execute(target, httpPost);
      } else {
        response = httpclient.execute(httpPost);
      }
      StatusLine statusLine = response.getStatusLine();
      sLogger.debug("statusCode = " + statusLine.getStatusCode());
      if (statusLine.getStatusCode() != 200) {
        throw new Exception(statusLine.toString());
      }
      HttpEntity entity = response.getEntity();
      responseBody = EntityUtils.toString(entity);
      // System.out.println("responseBody = " + responseBody);
      EntityUtils.consume(entity);
    } finally {
      httpPost.releaseConnection();
    }
  }



  private String buildUrl(PatrolEvent event, String description) throws Exception {
    int priority = DEFAULT_TRAC_PRIORITY; // _EMERGENCY, _HIGH, _MEDIUM, _LOW
    int district = 0; // in theory we should be able to determine this in many cases based on route and milepost, where known.
    StringBuilder sb = new StringBuilder();
    sLogger.debug("this.tracUrl = " + this.tracUrl);
    sb.append(this.tracUrl);
    sb.append("?priority=").append(priority);
    sb.append("&source=").append("WHP");
    sb.append("&district=").append(district);
    sb.append("&description=").append(URLEncoder.encode(description, ENCODING));
    // sb.append("&url=").append(ticketUrl);
    sb.append("&createdBy=WHP");
    return sb.toString();
  }


  private static final boolean ADD_ONLY_ONE_AT_A_TIME = false;
  private boolean addedOne = false; // Only add the first one, to slow things down during debugging.

  public void addToDb(DatabaseReader reader, PatrolEvent event) throws Exception {
    // Generate a new primary key long int
    long pk = KeyGenerator.nextVal(reader, this.sequenceName);
    event.setPk(pk);
    sLogger.trace("new PK = " + event.getPk());
    if (ADD_ONLY_ONE_AT_A_TIME && addedOne) {
      sLogger.debug("Already added one.  Skipping for now.");
    } else {
      event.insertRow(reader, this.tableName);
      addedOne = true;
      sLogger.debug("Added new patrol event " + event.getEvent() + " to " + this.tableName);
    }
  }


  /**
    Remove everything except the event ID number, created date, and updated date,
    because there's no need to save this to the patrol events table, and some of
    it could be PII (personally identifiable information), which we don't want
    to store if we don't have to.
   */
   @Deprecated
  public static void minimize(PatrolEvent patrolEvent) {
    patrolEvent.agency(null);
    patrolEvent.type(null);
    patrolEvent.description(null);
    patrolEvent.county(null);
    patrolEvent.direction(null);
    patrolEvent.street(null);
    patrolEvent.refMarker(null);
    patrolEvent.city(null);
    patrolEvent.crossStreet1(null);
    patrolEvent.crossStreet2(null);
    patrolEvent.comments(null);
  }


  /*
  public static void processEvents(JSONObject jo) {
    JSONArray events = jo.getJSONArray("Events");
    if (false) {
      for(int j = 0; j < events.length(); j++) {
        System.out.println("events[" + j + "] = " + events.getJSONObject(j).toString(2));
      }
    }
    if (true) {
      StringBuilder sb = new StringBuilder();
      //           000 P2016057132 2016-05-16 12:39 2016-05-16 12:39 XXXXXX 00000 X----------+
      sb.append("\n-N- -Event-ID-- ----Created----- ----Updated----- Agency CSize Description");
      for(int j = 0; j < events.length(); j++) {
        JSONObject event = events.getJSONObject(j);
        String eventId = event.getString("Event");
        String created = event.getString("Created");
        String updated = event.optString("Updated");
        String description = event.getString("Description");
        String agency = event.getString("Agency");
        String comments = event.getString("Comments");
        int size = 0;
        if (comments != null) {
          size = comments.length();
        }
        sb.append(String.format("\n%3d %11s %16s %16s %-6s %5d %s", j, eventId, created, updated, agency, size, description));
        comments = cleanComments(comments);
        sb.append('\n').append(comments + "\n");
      }
      sLogger.debug(sb.toString());
    }
  }
  */


  private static final boolean REMOVE_SOME_LINES = true;

  // private static final String REMOVABLE_LINE_PAT = "^\\*\\*\\s?(LOI|VEH|PER|&gt;&gt;&gt;&gt;)\\s+.*";
  private static final String REMOVABLE_LINE_PAT = "^\\*\\*\\s?(LOI|VEH|PER)\\s+.*";

  /**
    Take the raw comments as they come from the WHP web service and clean them up, to include:
      Replace | (pipe) chars with \n (newline)
      Prepend a bracketed line number to each line, eg. [1], [2], etc.
      Sanitize each line by replacing anything that looks like a phone number with {phone}
   */
  public static String cleanComments(String before) {
    String after = null;
    if (before != null) {
      if (true) {
        // Break it into lines, so we can look at each one individually?
        after = before.replaceAll("^\\|\\s*", ""); // replace leading | with nothing
        String[] lines = after.split("\\|");
        StringBuilder sb = new StringBuilder();
        for(int j = 0; j < lines.length; j++) {
          lines[j] = lines[j].trim();
          String line = lines[j];
          sLogger.debug("line before: " + DarUtils.qs(line));
          if (REMOVE_SOME_LINES && line.matches(REMOVABLE_LINE_PAT)) {
            sLogger.debug("Skipping: " + line);
            // skip it
            line = "XX " + line;
          } else {
            if (j > 0) {
              sb.append("\n");
            }
            line = sanitize(line);
            sb.append("[").append(j+1).append("] ").append(line);
            // System.out.println("lines[" + j + "]=" + line);
          }
          sLogger.debug(" line after: " + DarUtils.qs(line));
        }
        after = sb.toString();
      }
    }
    sLogger.debug("after:\n" + after);
    return after;
  }


  public static String sanitize(String before) {
    String after = null;
    // ten digits at the start of a string followed by a word break.
    after = before.replaceAll("^\\(?\\d{3}(\\)|-|\\s){0,2}\\d{3}(-|\\s)?\\d{4}\\b", "{phone}");
    // ten digits following anything but U, F, G, A, P, S, B (based on analysis of event IDs as of 2016 Jun 01
    after = after.replaceAll("([^ABFGPSU\\d])\\(?\\d{3}(\\(|\\)|-|\\s){0,2}\\d{3}(-|\\s)?\\d{4}\\b", "$1{phone}");
    // seven digits not following a P with a word break after.
    // A = ?
    // B = BRAND
    // F = ?
    // G = GF = Wyo Game and Fish, presumably
    // P = WHP = Wyo Highway Patrol
    // S = PARKS
    // U = ?
    after = after.replaceAll("([^ABFGPSU\\d])\\d{3}(-|\\s)?\\d{4}\\b", "$1{phone}");
    // Seven-digit phone numbers
    after = after.replaceAll("\\b\\d{3}(-|\\s)?\\d{4}\\b", "{phone}");
    if (OBSCURE_DISPATCHER_NAMES) {
      // WHP dispatcher/operator nameDoesntMatter
      after = after.replaceAll("(\\*\\* (>|&gt;){4} by: )([A-Za-z\\.\\-\\s]+) (on terminal:.*)", "$1{dispatcher} $4");
    }
    return after;
  }


  private List<PatrolEvent> getOldEvents(DatabaseReader pReader, SqlSelect pSelect) throws Exception {
    List<PatrolEvent> events = PatrolEvent.getMany(pReader, pSelect);
    if (true) {
      sLogger.debug("Found " + events.size() + " database events.");
    }
    return events;
  }

  private void addMessage(String msg) {
    if (this.messages == null) {
      messages = new ArrayList<String>();
    }
    messages.add(msg);
  }


  protected void updateHeartbeat() throws Exception {
    // open a reader
    // write a record to the heartbeats table
    Whenstamp now = new Whenstamp();
    DatabaseReader reader = null;
    try {
      reader = getReader(this.props);
      sLogger.debug("this.watchDogHeartBeatName = " + DarUtils.qs(this.watchDogHeartBeatName));
      WatchDogHeartBeat.update(reader, this.watchDogHeartBeatTableName, this.watchDogHeartBeatName, now, watchDogHeartBeatMaxAgeSecs);
    } catch (Exception ex) {
      sLogger.error("Failed to write heart beat: ", ex);
    } finally {
      // close the reader
      DarUtils.close(reader);
    }
  }


  static void highlight(PatrolEvent patrolEvent) {
    if (s_tracHighlighter == null) {
      sLogger.warn("s_tracHighlighter is null.  Skipping highlighting");
    } else {
      // patrolEvent.setDescription(s_tracHighlighter.highlight(patrolEvent.getDescription()));
      patrolEvent.setComments(s_tracHighlighter.highlight(patrolEvent.getComments()));
    }
  }


  private static List<PatrolEvent> removeUninterestingEvents(List<PatrolEvent> before) {
    List<PatrolEvent> after = null;
    if (before != null) {
      after = new ArrayList<PatrolEvent>();
      for(PatrolEvent event: before) {
        if (isInteresting(event)) {
          after.add(event);
        } else {
          sLogger.debug("Removing uninteresting event: " + event);
        }
      }
    }
    return after;
  }


  private static final String[] UNINTERESTING_TYPES = new String[] {"10-46"};


  private static boolean isInteresting(PatrolEvent event) {
    boolean result = true;
    if (event == null) {
      result = false;
    } else {
      result = isInteresting(event.getType());
    }
    return result;
  }


  private static boolean isInteresting(String type) {
    boolean result = true;
    if (type == null) {
      result = false;
    } else {
      for(String ut: UNINTERESTING_TYPES) {
        if (type.equals(ut)) {
          result = false;
        }
      }
    }
    sLogger.info("Type " + type + " is interesting? " + result);
    return result;
  }


}
