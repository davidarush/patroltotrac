package wyoroad.patrolToTrac;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import wyoroad.wydgets.DarUtils;
import wyoroad.wydgets.Properties;
import wyoroad.wydata.PatrolEvent;
import wyoroad.wydata.TracTask;
import java.util.List;
import java.util.ArrayList;
import wyoroad.wydgets.WebService;
import java.net.URL;
import java.io.File;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONTokener;
import org.json.JSONException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.http.StatusLine;

/*
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;

//import org.apache.commons.io.ReaderInputStream;
import org.apache.commons.io.IOUtils;

import java.security.cert.X509Certificate;
import java.security.cert.CertificateException;
*/

import java.net.URLDecoder;
import java.net.URI;
import java.util.Set;
import java.util.HashSet;
import wyoroad.wydgets.FileStuff;
import wyoroad.wydgets.DatabaseReader;
import wyoroad.wydgets.DarUtils;
import wyoroad.scrambler.Scrambler;
import wyoroad.scrambler.CipherText;
import wyoroad.scrambler.PlainText;
import org.apache.log4j.Logger;
import wyoroad.wydgets.sql.SqlSelect;
import wyoroad.wydgets.sql.SqlTableName;
import wyoroad.wydgets.sql.SqlSequenceName;
import wyoroad.wydgets.KeyGenerator;
import java.util.Map;
import java.util.HashMap;
import wyoroad.wydgets.Whenstamp;
import java.net.URLEncoder;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.StringReader;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.ContentType;
import wyoroad.wydata.WatchDogHeartBeat;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import wyoroad.wydgets.sql.SqlUtils;

public class AddCity {
  private static final Logger sLogger = Logger.getLogger(AddCity.class);
  private static final String ENCODING = "UTF-8";
  private static Scrambler s_scrambler = null;

  private Properties props = null;
  private String dbUrl = null;
  private String dbUsername = null;
  private PlainText dbPassword = null;
  private String abbrev = null; // WHP's city abbreviation
  private int district = 0;
  private String city = null; // full city name


  public static void main(String[] args) {
    try {
      sLogger.info("\n\nmain() starting");
      AddCity t = null;
      if (args.length > 2) {
        String abbrev = args[0];
        int district = Integer.parseInt(args[1]);
        String city = args[2];
        t = new AddCity(abbrev, district, city);
      } else {
        t = new AddCity();
      }
      t.go();
    } catch (Exception ex) {
      sLogger.error("Caught in main: ", ex);
      ex.printStackTrace(System.err);
      System.exit(1);
    }
    System.exit(0);
  }

  protected SqlTableName cityToDistrictTableName = null;
  protected SqlSelect cityToDistrictSelect = null;


  public AddCity() throws Exception {
    build();
  }


  public AddCity(String abbrev, int district, String city) throws Exception {
    this.abbrev = abbrev;
    this.district = district;
    this.city = city;
    build();
  }


  protected void build() throws Exception {
    this.props = new Properties(new File("patrolToTrac.properties"));
    this.dbUrl = props.get("db_url");
    this.dbUsername = props.get("db_username");
    this.dbPassword = new PlainText(Scrambler.Decrypt(new CipherText(props.get("db_password"))));

    this.cityToDistrictSelect  = new SqlSelect(props.get("patrol_city_district_select"));
    this.cityToDistrictTableName  = new SqlTableName(props.get("patrol_city_district_table_name"));
  }



  private DatabaseReader getReader() throws Exception {
    DatabaseReader reader = null;
    sLogger.debug("dbUrl = " + this.dbUrl);
    sLogger.debug("dbUsername = " + this.dbUsername);
    reader = new DatabaseReader(this.dbUrl, this.dbUsername, this.dbPassword);
    reader.setTracer("AddCity");
    reader.open();
    return reader;
  }


  public void go() throws Exception {
    DatabaseReader reader = null;
    PreparedStatement ps = null;
    int count = 0;
    try {
      reader = getReader();
      if (abbrev != null) {
        java.sql.Connection conn = reader.getConnection();
        String sql = "insert into " + cityToDistrictTableName + " (ABBREV,DISTRICT,CITY) values(?,?,?)";
        System.out.println("sql = " + sql);
        ps = conn.prepareStatement(sql);
        ps.setString(1, this.abbrev);
        ps.setInt(2, this.district);
        ps.setString(3, this.city);
        count = ps.executeUpdate();
        System.out.println("count of added rows: " + count);
      }
      list(reader);
    } finally {
      if (ps != null) {
        ps.close();
      }
      DarUtils.close(reader);
    }
  }

  private void list(DatabaseReader reader) throws Exception {
    ResultSet rs = null;
    try {
      rs = reader.execute(this.cityToDistrictSelect);
      int j = 0;
      while (rs.next()) {
        String a = rs.getString(1);
        int d = rs.getInt(2);
        String c = rs.getString(3);
        System.out.printf("%3d %-7s %1d %s\n", j, a, d, c);
        j++;
      }
    } finally {
      SqlUtils.closeStmtToo(rs);
    }
  }


}
