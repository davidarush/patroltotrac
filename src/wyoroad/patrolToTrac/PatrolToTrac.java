package wyoroad.patrolToTrac;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import wyoroad.wydgets.DarUtils;
import wyoroad.wydgets.DatabaseReader;
import wyoroad.wydata.PatrolEvent;
import wyoroad.wydata.TracTask;
import wyoroad.wydgets.Properties;
import wyoroad.scrambler.Scrambler;
import wyoroad.scrambler.CipherText;
import wyoroad.scrambler.PlainText;
import wyoroad.wydgets.sql.SqlSelect;
import wyoroad.wydgets.sql.SqlTableName;
import wyoroad.wydgets.sql.SqlSequenceName;
import wyoroad.wydgets.KeyGenerator;
import wyoroad.wydgets.WebService;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONTokener;
import org.json.JSONException;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

/**
 @deprecated use PatrolToTrac2 instead.
 */
@Deprecated
public class PatrolToTrac {
  private static final Logger sLogger = Logger.getLogger(PatrolToTrac.class);

  // private List<PatrolEvent> fileEvents = null;
  // private List<PatrolEvent> dbEvents = null;


  public static void main(String[] args) throws Exception {
    PatrolToTrac t = new PatrolToTrac();
    t.go();
  }


  public PatrolToTrac() {
  }


  private static final boolean IGNORE_ALL_DUPLICATES = true;
  private static final boolean DO_FTP = false;
  private static final boolean DO_WEB_SERVICE_GET = true;
  private static final boolean REPORT = false;

  public void go() throws Exception {
    sLogger.info("\n\nStarting.");
    String tmp = null;
    // 1. Read properties from .properties file
    // 2. Issue FTP job to grab new patrol events, saved to file.
    // 3. Read events from the FTPed file.
    // 4. Read events from the database.
    // 5. Look at each file event:
    //    5.1. If the event number is already in the database and the updated timestamp in the file is greater (newer) than the updated timestamp in the database, update the record in the database.
    //    5.2. If the event from the file is not found in the database, insert it.
    //    5.3. Otherwise ignore it.

    // 1. Read properties
    String pfilename = "patrolToTrac.properties";
    sLogger.debug("Reading properties from " + DarUtils.quotedString(pfilename));
    Properties props = new Properties(new File(pfilename));

    String dbUrl = props.get("db_url");
    sLogger.debug("dbUrl = " + dbUrl);

    String dbUsername = props.get("db_username");
    sLogger.debug("dbUsername = " + dbUsername);

    PlainText dbPassword = new PlainText(Scrambler.Decrypt(new CipherText(props.get("db_password"))));
    // sLogger.debug("dbPassword = " + dbPassword);

    SqlSelect select = new SqlSelect(props.get("patrol_events_select"));
    sLogger.debug("select = " + select);

    SqlTableName tableName = new SqlTableName(props.get("patrol_events_table_name"));
    sLogger.debug("tableName = " + tableName);

    SqlSequenceName sequenceName = new SqlSequenceName(props.get("patrol_events_sequence_name"));
    sLogger.debug("sequenceName = " + sequenceName);

    String ftpHostname = props.get("ftp_hostname");
    sLogger.debug("ftpHostname = " + ftpHostname);

    String ftpUsername = props.get("ftp_username");
    sLogger.debug("ftpUsername = " + ftpUsername);

    PlainText ftpPassword = new PlainText(Scrambler.Decrypt(new CipherText(props.get("ftp_password"))));
    sLogger.debug("ftpPassword = " + ftpPassword);

    String eventsFileName = props.get("patrol_events_file_name");
    sLogger.debug("eventsFileName = " + eventsFileName);

    sLogger.info("Might getting file " + eventsFileName + " via FTP from " + ftpHostname);
    sLogger.info("Using table " + tableName + " as " + dbUsername + " on " + dbUrl);

    String tracUrl = props.get("trac_newtask_url");

    sLogger.info("Will report new and updated patrol events to TRAC via: " + tracUrl);

    String eventsUrl = props.get("patrol_events_url");
    sLogger.info("Patrol eventsUrl =  " + eventsUrl);

    List<PatrolEvent> fileEvents = null;

    if (DO_FTP) {
      // 2. Do FTP get
      ftpEventsFile(ftpHostname, ftpUsername, ftpPassword, eventsFileName);

      // 3. Read patrol events from the FTPed file
      fileEvents = getEvents(new File(eventsFileName)); // "Events_TMC_1.txt"));
      sLogger.info("Read " + fileEvents.size() + " events from file " + eventsFileName);
    } else if (DO_WEB_SERVICE_GET) {
      fileEvents = getEvents(new URL(eventsUrl));
      sLogger.info("Read " + fileEvents.size() + " events from URL " + eventsUrl);
    } else {
      sLogger.error("No DO_* method is true.");
    }

    if (fileEvents.size() <= 0) {
      sLogger.info("No events to process.");
    } else {
      int j = 0;
      if (true) {
        for(PatrolEvent pe: fileEvents) {
          sLogger.debug(String.format("%2d: %s\n", j++, pe.toStringShort()));
        }
      }

      DatabaseReader reader = null;
      try {
        reader = new DatabaseReader(dbUrl, dbUsername, dbPassword);
        reader.setTracer("PatrolToTrac-getEvents");
        reader.open();

        // 4. Read events (map) from the database.
        Map<String,PatrolEvent> dbEvents = getEvents(reader, select);
        sLogger.info("Read " + dbEvents.size() + " events from table " + tableName);

        // 5. For each file event...
        for(PatrolEvent fe: fileEvents) {
          String key = fe.getEvent();
          if (dbEvents.containsKey(key)) {
            if (IGNORE_ALL_DUPLICATES) {
              sLogger.info("Ignoring duplicate event, regardless of timestamp: " + fe.getEvent());
            } else {
              PatrolEvent de = dbEvents.get(key);
              // if the de updated is null but fe updated is not, update the DB
              if (de.getUpdated() == null) {
                if (fe.getUpdated() != null) {
                  tmp = "Event " + key + " now has an updated timestamp.  Updating it.";
                  sLogger.debug(tmp);
                  fe.setPk(de.getPk());  // copy the old PK
                  fe.updateRow(reader, tableName);
                  report(tracUrl, "Updated", fe);
                } else {
                  // both null, nothing to do
                  sLogger.debug("Both updated dates are null, nothing to do.");
                }
              } else {
                // de.getUpdated() is not null
                if (fe.getUpdated() != null) {
                  // neither is null
                  if (fe.getUpdated().isAfter(de.getUpdated())) {
                    sLogger.debug("Event " + fe.getEvent() + " has a newer updated date, updating it.");
                    fe.setPk(de.getPk());  // copy the old PK
                    fe.updateRow(reader, tableName);
                    report(tracUrl, "Updated", fe);
                  } else {
                    sLogger.debug("Updated in file is not newer than updated in db.");
                  }
                } else {
                  // fe.getUpdated() is null
                  tmp = "Event " + key + " has null updated from file, but non-null updated from database.  Odd, but nothing to do.";
                  sLogger.debug(tmp);
                }
              } // else
            }// else if IGNORE_ALL_DUPLICATES
          } else {
            tmp = "Event " + key + " from ftp/file is new.  Adding it to database.";
            sLogger.debug(tmp);
            // Generate a new primary key long int
            long pk = KeyGenerator.nextVal(reader, sequenceName);
            fe.setPk(pk);
            fe.insertRow(reader, tableName);
            report(tracUrl, "New", fe);
          } // else
        } // for
      } finally {
        DarUtils.close(reader);
      }
    }
    sLogger.info("Done.");
  }


  private void report(String tracUrl, String reason, PatrolEvent event) throws Exception {
    // Report this new or updated event to TRAC
    final String ENCODING = "UTF-8";
    String tmp = "tracUrl = " + tracUrl;
    sLogger.debug(tmp);
    sLogger.info("  To TRAC, " + reason + ":" + event.toStringShort());
    int priority = TracTask.PRIORITY_HIGH; // _EMERGENCY, _HIGH, _MEDIUM, _LOW
    int district = 0;
    StringBuilder sb = new StringBuilder();
    // build the description string
    sb.append("<span style=\"font-weight:bold\">WHP Event ").append(event.getEvent()).append(": ").append(event.getType()).append("</span>");
    sb.append(", ").append(event.getDescription());
    sb.append("<br/>Agency: ").append(event.getAgency());
    if (event.getCounty() != null) {
      sb.append(", County: ").append(event.getCounty());
    } else {
      // sb.append("<br/>");
    }
    if (event.getCity() != null) {
      sb.append(", City: ").append(event.getCity());
    }
    sb.append("<br/>Street: ").append(event.getStreet());
    sb.append(", Ref Marker: ").append(event.getRefMarker());
    if (event.getDirection() != null) {
      sb.append(", Direction: ").append(event.getDirection());
    }
    if (event.getCrossStreet1() != null) {
      sb.append(", Cross Street 1: ").append(event.getCrossStreet1());
    }
    if (event.getCrossStreet2() != null) {
      sb.append(", Cross Street 2: ").append(event.getCrossStreet2());
    }
    if (event.getCreated() != null) {
      sb.append("<br/>Created: ").append(event.getCreated().toString());
    } else {
      sb.append("<br/>");
    }
    if (event.getUpdated() != null) {
      sb.append(", Updated: ").append(event.getUpdated().toString());
    }
    String description = sb.toString();

    sb = new StringBuilder();
    sb.append(tracUrl);
    sb.append("?priority=").append(priority);
    sb.append("&source=").append("WHP");
    sb.append("&district=").append(district);
    sb.append("&description=").append(URLEncoder.encode(description, ENCODING));
    // sb.append("&url=").append(ticketUrl);
    sb.append("&createdBy=").append(URLEncoder.encode("WHP 1", ENCODING));
    String wholeUrl = sb.toString();
    sLogger.debug("Whole URL: " + wholeUrl);
    if (REPORT) {
      sLogger.debug("Sending message to TRAC: " + description);
      String response = WebService.getString(new URL(wholeUrl));
      sLogger.debug("Web service response: " + response);
    } else {
      sLogger.debug("Not actually reporting because REPORT = " + REPORT);
    }
  }


  /**
    Get events from the web service that WHP maintains.
    'Tis JSON format.
    */
  private List<PatrolEvent> getEvents(URL url) throws Exception {
    // look to see if it looks like an HTTP url or a FILE url...
    String urlStr = url.toString();
    sLogger.debug("urlStr = " + url);
    JSONObject jo = null;
    InputStream inputStream = null;
    if (urlStr.startsWith("http://") || urlStr.startsWith("https://")) {
      sLogger.debug("Looks like an HTTP(S) url: " + url);
      jo = new JSONObject(new JSONTokener(new InputStreamReader(url.openStream())));
    } else if (urlStr.startsWith("file://")) {
      sLogger.debug("looks like a file URL: " + url);
      String fileName = urlStr.substring(6);
      sLogger.debug("fileName = " + fileName);
      jo = new JSONObject(new JSONTokener(new FileInputStream(new File(fileName))));
    }
    if (jo != null) {
      sLogger.debug("jo = " + jo.toString(2));
    } else {
      sLogger.warn("jo is null");
    }
    List<PatrolEvent> patrolEvents = new ArrayList<PatrolEvent>();
    JSONArray jsonEvents = jo.getJSONArray("Events");
    String key = null;
    for(int j = 0; j < jsonEvents.length(); j++) {
      JSONObject jsonEvent = jsonEvents.getJSONObject(j);
      System.out.println("jsonEvent [" + j + "] = " + jsonEvent.toString(2));
      System.out.println("Event: " + getStr(jsonEvent, "Event"));
      System.out.println("  Description: " + getStr(jsonEvent, "Description"));
      System.out.println("  Type:        " + jsonEvent.getString("Type"));
      System.out.println("  Created:     " + getStr(jsonEvent, "Created"));
      System.out.println("  Updated:     " + getStr(jsonEvent, key));
      System.out.println("  Street:      " + getStr(jsonEvent, "Street"));
      System.out.println("  Dir:         " + getStr(jsonEvent, key));
      System.out.println("  RM:          " + getStr(jsonEvent, "Reference Marker"));
      System.out.println("  City:        " + getStr(jsonEvent, "City"));
      System.out.println("  County:      " + getStr(jsonEvent, "County"));
      System.out.println("  Agency:      " + getStr(jsonEvent, "Agency"));
      System.out.println("  Closed:      " + getStr(jsonEvent, "Closed"));
      System.out.println("  Comments:    " + getStr(jsonEvent, "Comments"));
    }
    // Populate them HERE from the jo JSONObject
    return patrolEvents;
  }


  private static String getStr(JSONObject jo, String key) throws JSONException {
    if (jo.isNull(key)) {
      return null;
    }
    return jo.getString(key);
  }


  private List<PatrolEvent> getEvents(InputStream inStream) throws Exception {
    sLogger.debug("Getting events from inStream " + inStream);
    List<PatrolEvent> events = new ArrayList<PatrolEvent>();
    JSONTokener tokener = new JSONTokener(inStream);
    JSONObject root = new JSONObject(tokener);
    sLogger.debug("root = " + root);
    /*
    sLogger.debug("Found " + events.size() + " file events.");
    if (true) {
      int j = 0;
      for(PatrolEvent pe: events) {
        sLogger.debug(String.format("[%2d]: %s\n", j++, pe.toString()));
      }
    }
    */
    return events;
  }


  /**
    Get events from the file that WHP updates every few minutes, and that we got via FTP.
    */
  private List<PatrolEvent> getEvents(File file) throws Exception {
    List<PatrolEvent> events = new ArrayList<PatrolEvent>();
    BufferedReader br = null;
    try {
      br = new BufferedReader(new FileReader(file));
      String line = null;
      while ( (line=br.readLine()) != null) {
        line = cleanup(line); // strip out all the garbage
        if (line.matches("^[A-Z]2\\d{9}\\s.*")) {
          try {
            PatrolEvent pe = new PatrolEvent(line);
            String type = pe.getType();
            if ("10-46".equals(type)) {
              // ignore motorist assists.
              sLogger.info("Ignoring 10-46: " + pe.getEvent() + " at " + pe.getCreated());
            } else {
              events.add(pe);
            }
            sLogger.trace("pe = " + pe);
          } catch (Exception ex) {
            sLogger.error("Caught exception trying to decypher patrol event from line = " + DarUtils.quotedString(line));
          }
        }
      }
    } finally {
      if (br != null) {
        br.close();
      }
    }
    sLogger.debug("Found " + events.size() + " file events.");
    if (true) {
      int j = 0;
      for(PatrolEvent pe: events) {
        sLogger.debug(String.format("[%2d]: %s\n", j++, pe.toString()));
      }
    }
    return events;
  }


  private Map<String,PatrolEvent> getEvents(DatabaseReader pReader, SqlSelect pSelect) throws Exception {
    Map<String,PatrolEvent> map = PatrolEvent.getMapByEvent(pReader, pSelect);
    if (true) {
      sLogger.debug("Found " + map.size() + " database events.");
      if (false) {
        int j = 0;
        for(String key: map.keySet()) {
          sLogger.debug(String.format("%2d: %s\n", j++, map.get(key).toStringShort()));
        }
      }
    }
    return map;
  }


  private int counter = 0;
  private static final int LEN = 60;

  private String cleanup(String before) {
    StringBuilder sb = new StringBuilder();
    for(int j = 0; j < before.length(); j++) {
      char c = before.charAt(j);
      if (c > 0) {
        sb.append(c);
      }
    }
    return sb.toString();
  }


  private void ftpEventsFile(String hostname, String username, PlainText password, String fileName) throws Exception {
    sLogger.debug("FTPing to host " + hostname);
    FTPClient ftp = new FTPClient();
    int reply = 0;
    String tmp = null;
    try {
      ftp.connect(hostname);
      reply = ftp.getReplyCode();
      tmp = "Connect reply string: " + ftp.getReplyString();
      sLogger.debug(tmp);
      if (FTPReply.isPositiveCompletion(reply)) {
        if (ftp.login(username, password.toString())) {
          tmp = "Remote system is " + ftp.getSystemType();
          sLogger.debug(tmp);
          OutputStream output = new FileOutputStream(fileName);
          ftp.retrieveFile(fileName, output);
          output.close();
          tmp = "Retrieved file " + fileName;
          sLogger.debug(tmp);
        } else {
          tmp = "Login failed to " + hostname + " as " + username;
          sLogger.error(tmp);
          System.err.println(tmp);
        }
        ftp.logout();
      } else {
        tmp = "Failed to connect to " + hostname;
        sLogger.error(tmp);
        System.err.println(tmp);
      }
    } finally {
      if (ftp != null) {
        ftp.disconnect();
      }
    }
  }

}
