package wyoroad.patrolToTrac;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import wyoroad.wydgets.DarUtils;
import wyoroad.wydgets.Properties;
import wyoroad.wydata.PatrolEvent;
import wyoroad.wydata.TracTask;
import java.util.List;
import java.util.ArrayList;
import wyoroad.wydgets.WebService;
import java.net.URL;
import java.io.File;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONTokener;
import org.json.JSONException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.http.StatusLine;

import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;

//import org.apache.commons.io.ReaderInputStream;
import org.apache.commons.io.IOUtils;

import java.security.cert.X509Certificate;
import java.security.cert.CertificateException;

import java.net.URLDecoder;
import java.net.URI;
import java.util.Set;
import java.util.HashSet;
import wyoroad.wydgets.FileStuff;
import wyoroad.wydgets.DatabaseReader;
import wyoroad.wydgets.DarUtils;
import wyoroad.scrambler.Scrambler;
import wyoroad.scrambler.CipherText;
import wyoroad.scrambler.PlainText;
import org.apache.log4j.Logger;
import wyoroad.wydgets.sql.SqlSelect;
import wyoroad.wydgets.sql.SqlTableName;
import wyoroad.wydgets.sql.SqlSequenceName;
import wyoroad.wydgets.KeyGenerator;
import java.util.Map;
import java.util.HashMap;
import wyoroad.wydgets.Whenstamp;
import java.net.URLEncoder;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.StringReader;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.ContentType;
import wyoroad.wydata.WatchDogHeartBeat;

public class CheckWatchDogHeartBeat {
  private static final Logger sLogger = Logger.getLogger(CheckWatchDogHeartBeat.class);


  public static void main(String[] args) {
    try {
      sLogger.info("\n\nmain() starting");
      CheckWatchDogHeartBeat t = null;
      t = new CheckWatchDogHeartBeat();
      t.go();
    } catch (Exception ex) {
      sLogger.error("Caught in main: ", ex);
      ex.printStackTrace(System.err);
      System.exit(1);
    }
    System.exit(0);
  }


  protected URI uri = null;
  protected File file = null;
  protected SqlTableName tableName = null;
  protected SqlSequenceName sequenceName = null;
  protected SqlSelect select = null;
  protected String tracUrl = null;
  protected List<String> messages = null;
  protected Scrambler scrambler = null;

  protected String dbUrl = null;
  protected String dbUsername = null;
  protected PlainText dbPassword = null;
  protected Properties props = null;
  protected SqlTableName watchDogHeartBeatTableName = null;
  protected String watchDogHeartBeatName = null;
  protected int watchDogHeartBeatMaxAgeSecs = 0;


  public CheckWatchDogHeartBeat() throws Exception {
    build();
  }


  protected void build() throws Exception {
    this.props = new Properties(new File("patrolToTrac.properties"));
    this.dbUrl = props.get("db_url");
    this.dbUsername = props.get("db_username");
    this.dbPassword = new PlainText(Scrambler.Decrypt(new CipherText(props.get("db_password"))));
    this.watchDogHeartBeatTableName  = new SqlTableName(props.get("watch_dog_heart_beat_table_name"));
    this.watchDogHeartBeatName       = props.get("watch_dog_heart_beat_name");
    this.watchDogHeartBeatMaxAgeSecs = props.getInt("watch_dog_heart_beat_max_age_secs", 310);
  }


  public void go() throws Exception {
    // open a reader
    // read a record to the heartbeats table
    Whenstamp now = new Whenstamp();
    DatabaseReader reader = null;
    try {
      reader = getReader(this.props);
      sLogger.debug("this.watchDogHeartBeatName = " + DarUtils.qs(this.watchDogHeartBeatName));
      SqlSelect select = new SqlSelect("select * from " + this.watchDogHeartBeatTableName + " where NAME='" + this.watchDogHeartBeatName + "'");
      System.out.println("select = " + select);
      WatchDogHeartBeat x = WatchDogHeartBeat.getFirst(reader, select);
      System.out.println("now = " + now);
      System.out.println("x = " + x);
      long ageSecs = x.getAgeSecs();
      int secs = (int)(ageSecs % 60);
      int mins = (int)((ageSecs - secs) / 60);
      System.out.printf("Age is %d seconds = %d:%02d\n", ageSecs, mins, secs);
    } catch (Exception ex) {
      sLogger.error("Failed to read heart beat: ", ex);
    } finally {
      // close the reader
      DarUtils.close(reader);
    }
  }


  private DatabaseReader getReader(Properties props) throws Exception {
    DatabaseReader reader = null;
    //String dbUrl = props.get("db_url");
    sLogger.debug("dbUrl = " + this.dbUrl);

    //String dbUsername = props.get("db_username");
    sLogger.debug("dbUsername = " + this.dbUsername);

    //PlainText dbPassword = new PlainText(Scrambler.Decrypt(new CipherText(props.get("db_password"))));
    // sLogger.debug("dbPassword = " + dbPassword);

    reader = new DatabaseReader(this.dbUrl, this.dbUsername, this.dbPassword);
    reader.setTracer("PatrolToTrac2-CheckWatchDogHeartBeat");
    reader.open();
    return reader;
  }

}
