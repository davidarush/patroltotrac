package wyoroad.patrolToTrac;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import wyoroad.wydgets.DarUtils;
import wyoroad.wydgets.Properties;
import wyoroad.wydata.PatrolEvent;
import wyoroad.wydata.TracTask;
import java.util.List;
import java.util.ArrayList;
import wyoroad.wydgets.WebService;
import java.net.URL;
import java.io.File;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONTokener;
import org.json.JSONException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.http.StatusLine;

import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;

//import org.apache.commons.io.ReaderInputStream;
import org.apache.commons.io.IOUtils;

import java.security.cert.X509Certificate;
import java.security.cert.CertificateException;

import java.net.URLDecoder;
import java.net.URI;
import java.util.Set;
import java.util.HashSet;
import wyoroad.wydgets.FileStuff;
import wyoroad.wydgets.DatabaseReader;
import wyoroad.wydgets.DarUtils;
import wyoroad.scrambler.Scrambler;
import wyoroad.scrambler.CipherText;
import wyoroad.scrambler.PlainText;
import org.apache.log4j.Logger;
import wyoroad.wydgets.sql.SqlSelect;
import wyoroad.wydgets.sql.SqlTableName;
import wyoroad.wydgets.sql.SqlSequenceName;
import wyoroad.wydgets.KeyGenerator;
import java.util.Map;
import java.util.HashMap;
import wyoroad.wydgets.Whenstamp;
import java.net.URLEncoder;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.StringReader;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.ContentType;
import wyoroad.wydata.WatchDogHeartBeat;
import java.sql.ResultSet;

public class AnalyzeOldEvents {
  private static final Logger sLogger = Logger.getLogger(AnalyzeOldEvents.class);
  private static final String ENCODING = "UTF-8";


  public static void main(String[] args) {
    try {
      sLogger.info("\n\nmain() starting");
      // Look at the first command line argument.  Is it a URL?  If not, assume it's a file.
      AnalyzeOldEvents t = null;
      t = new AnalyzeOldEvents();
      t.go();
    } catch (Exception ex) {
      sLogger.error("Caught in main: ", ex);
      ex.printStackTrace(System.err);
      System.exit(1);
    }
    System.exit(0);
  }


  protected SqlTableName eventsTableName = null;
  protected SqlSequenceName sequenceName = null;
  protected SqlSelect eventsSelect = null;

  protected String dbUrl = null;
  protected String dbUsername = null;
  protected PlainText dbPassword = null;
  protected Properties props = null;


  public AnalyzeOldEvents() throws Exception {
    // Will pull URI from properties file later.
    build();
  }


  protected void build() throws Exception {
    this.props = new Properties(new File("patrolToTrac.properties"));
    this.dbUrl = props.get("db_url");
    this.dbUsername = props.get("db_username");
    this.dbPassword = new PlainText(Scrambler.Decrypt(new CipherText(props.get("db_password"))));
    this.eventsSelect = new SqlSelect(props.get("patrol_events_select"));
    this.eventsTableName = new SqlTableName(props.get("patrol_events_table_name"));
    // this.sequenceName = new SqlSequenceName(props.get("patrol_events_sequence_name"));
  }




  public void go() throws Exception {
    // Open reader
    // Get all patrolEvents
    // For each one, if age is > 24 hours, remove it from the DB

    // Grab "old" events from the DB
    DatabaseReader reader = null;
    try {
      reader = getReader(props);
      List<PatrolEvent> oldEvents = getOldEvents(reader, this.eventsSelect);
      analyzeOldEvents(oldEvents);
    } finally {
      DarUtils.close(reader);
    }
  }


  private DatabaseReader getReader(Properties props) throws Exception {
    DatabaseReader reader = null;
    //String dbUrl = props.get("db_url");
    sLogger.debug("dbUrl = " + this.dbUrl);

    //String dbUsername = props.get("db_username");
    sLogger.debug("dbUsername = " + this.dbUsername);

    //PlainText dbPassword = new PlainText(Scrambler.Decrypt(new CipherText(props.get("db_password"))));
    // sLogger.debug("dbPassword = " + dbPassword);

    reader = new DatabaseReader(this.dbUrl, this.dbUsername, this.dbPassword);
    reader.setTracer("AnalyzeOldEvents-getEvents");
    reader.open();
    return reader;
  }


  /**
   */
  public void analyzeOldEvents(List<PatrolEvent> oldEvents) throws Exception {
    if (oldEvents == null) throw new NullPointerException("oldEvents is null");
    int j = 0;
    Set<Character> firstChars = new HashSet<Character>();
    for(PatrolEvent oldEvent: oldEvents) {
      System.out.printf("%6d: %s\n", j++, oldEvent.getEvent());
      char c = DarUtils.getFirstChar(oldEvent.getEvent());
      if (firstChars.contains(c)) {
        // do nothing
      } else {
        System.out.printf("New first char: %c\n", c);
        firstChars.add(c);
      }
    }
    System.out.print("New first chars: ");
    boolean first = true;
    for(char c: firstChars) {
      if (first) {
        first = false;
      } else {
        System.out.print(", ");
      }
      System.out.printf("%c", c);
    }
    System.out.println();
  }


  private List<PatrolEvent> getOldEvents(DatabaseReader pReader, SqlSelect pSelect) throws Exception {
    List<PatrolEvent> events = PatrolEvent.getMany(pReader, pSelect);
    return events;
  }


}
