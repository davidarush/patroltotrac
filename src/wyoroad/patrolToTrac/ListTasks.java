package wyoroad.patrolToTrac;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import wyoroad.wydgets.DarUtils;
import wyoroad.wydgets.Properties;
import wyoroad.wydata.PatrolEvent;
import wyoroad.wydata.TracTask;
import java.util.List;
import java.util.ArrayList;
import wyoroad.wydgets.WebService;
import java.net.URL;
import java.io.File;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONTokener;
import org.json.JSONException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.http.StatusLine;

import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;

//import org.apache.commons.io.ReaderInputStream;
import org.apache.commons.io.IOUtils;

import java.security.cert.X509Certificate;
import java.security.cert.CertificateException;

import java.net.URLDecoder;
import java.net.URI;
import java.util.Set;
import java.util.HashSet;
import wyoroad.wydgets.FileStuff;
import wyoroad.wydgets.DatabaseReader;
import wyoroad.wydgets.DarUtils;
import wyoroad.scrambler.Scrambler;
import wyoroad.scrambler.CipherText;
import wyoroad.scrambler.PlainText;
import org.apache.log4j.Logger;
import wyoroad.wydgets.sql.SqlSelect;
import wyoroad.wydgets.sql.SqlTableName;
import wyoroad.wydgets.sql.SqlSequenceName;
import wyoroad.wydgets.KeyGenerator;
import java.util.Map;
import wyoroad.wydgets.Whenstamp;
import java.net.URLEncoder;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.StringReader;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.ContentType;

public class ListTasks {
  private static final Logger sLogger = Logger.getLogger(ListTasks.class);
  private static final boolean WRITE_TEXT_FILE = true;
  private static final boolean REPORT_TO_TRAC = true;
  private static final boolean INCLUDE_COMMENTS_IN_TRAC_TASK = true;
  private static final int DEFAULT_TRAC_PRIORITY = TracTask.PRIORITY_HIGH;
  private static final String ENCODING = "UTF-8";
  public static final boolean OBSCURE_DISPATCHER_NAMES = false;

  public static void main(String[] args) {
    ListTasks x = null;
    try {
      x = new ListTasks();
      x.go();
    } catch (Exception ex) {
      sLogger.error("Caught in main: ", ex);
      ex.printStackTrace(System.err);
      System.exit(1);
    }
    System.exit(0);
  }


  protected SqlTableName tableName = null;
  protected SqlSequenceName sequenceName = null;
  protected SqlSelect select = null;
  protected List<TracTask> largeTasks = null;

  public ListTasks() {
    // Will pull URI from properties file later.
    this.largeTasks = new ArrayList<TracTask>();
  }

  private static byte[] WHP_COMMENTS_SCRAMBLER_KEY = {-96,-57,118,38,-47,14,49,-89,-108,-126,113,-95,118,56,115,15};

  public void go() throws Exception {
    Scrambler scrambler = new Scrambler(WHP_COMMENTS_SCRAMBLER_KEY);
    Properties props = new Properties(new File("patrolToTrac.properties"));
    this.select = new SqlSelect(props.get("patrol_events_select"));
    sLogger.debug("this.select = " + this.select);
    this.tableName = new SqlTableName(props.get("patrol_events_table_name"));
    sLogger.debug("this.tableName = " + this.tableName);
    this.sequenceName = new SqlSequenceName(props.get("patrol_events_sequence_name"));
    sLogger.debug("this.sequenceName = " + this.sequenceName);
    List<TracTask> tasks = getTasks(props);
    int j = 0;
    double largest = -1.0;
    double smallest = -1.0;
    for(TracTask task: tasks) {
      String desc = task.getDescription();
      int size = 0;
      if (desc != null) {
        size = desc.length();
      }
      String encrypted = scrambler.encrypt(new PlainText(task.getDescription()));
      int esize = encrypted.length();
      double percentage = (double) esize / (double) size;
      if (largest < 0.0 || percentage > largest) {
        largest = percentage;
      }
      if (smallest < 0.0 || percentage < smallest) {
        smallest = percentage;
      }
      System.out.printf("tasks[%d]: pk = %d, description size = %d, encrypted size = %d, %.3f\n", j++, task.getPk(), size, esize, percentage);
    }
    System.out.printf("smallest = %.4f, largest = %.4f\n", smallest, largest);
  }


  private List<TracTask> getTasks(Properties props) throws Exception {
    List<TracTask> tasks = null;
    DatabaseReader reader = null;
    SqlSelect select = new SqlSelect(props.get("trac_tasks_select"));
    try {
      reader = getReader(props);
      tasks = TracTask.getMany(reader, select);
    } finally {
      DarUtils.close(reader);
    }
    return tasks;
  }


  private static DatabaseReader getReader(Properties props) throws Exception {
    DatabaseReader reader = null;
    String dbUrl = props.get("db_url");
    sLogger.debug("dbUrl = " + dbUrl);

    String dbUsername = props.get("db_username");
    sLogger.debug("dbUsername = " + dbUsername);

    PlainText dbPassword = new PlainText(Scrambler.Decrypt(new CipherText(props.get("db_password"))));
    // sLogger.debug("dbPassword = " + dbPassword);

    reader = new DatabaseReader(dbUrl, dbUsername, dbPassword);
    reader.setTracer("PatrolToTrac-getEvents");
    reader.open();
    return reader;
  }

}
