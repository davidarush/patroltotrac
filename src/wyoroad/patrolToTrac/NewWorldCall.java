package wyoroad.patrolToTrac;
import java.io.IOException;
import java.io.File;
import java.io.InputStream;
import java.io.FileInputStream;
import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Elements;
import org.apache.log4j.Logger;


/**
  Represents one call record from Tyler Systems New World Computer-Aided Dispatch system.
  */
public class NewWorldCall {
  private static final Logger sLogger = Logger.getLogger(NewWorldCall.class);

  // private static String NAMESPACE = "";
  // private static String NAMESPACE = "http://www.w3.org/2001/XMLSchema-instance";
  private static String NAMESPACE = "http://www.newworldsystems.com/Aegis/CAD/Peripheral/CallExport/2011/02";

  protected Element call = null;
  protected String callId = null;
  protected String callNumber = null;
  protected String closedFlag = null;
  protected String canceledFlag = null;
  protected String createDateTime = null;
  protected Location location = null;
  protected String natureOfCall = null;


  public NewWorldCall(File xmlFile) throws Exception {
    this.parse(xmlFile);
  }

  public String toString() {
    return "CallId=" + this.callId 
         + ", callNumber=" + this.callNumber
         + ", createDateTime=" + this.createDateTime 
         + ", closedFlag=" + this.closedFlag 
         + ", canceledFlag=" + this.canceledFlag 
         + ",\n  natureOfCall=" + this.natureOfCall
         + ",\n  location=" + this.location
         ;
  }


  private void parse(File xmlFile) throws Exception {
    FileInputStream fis = null;
    try {
      fis = new FileInputStream(xmlFile);
      this.parse(fis);
    } finally {
      if (fis != null) {
        fis.close();
      }
    }
  }


  private void parse(InputStream inStream) throws Exception {
    Builder parser = new Builder();
    Document doc = parser.build(inStream);
    this.call = doc.getRootElement();
    sLogger.debug("call's NamespaceURI = " + this.call.getNamespaceURI());

    logChild(this.call, "CallId");
    logChild(this.call, "CallNumber");
    logChild(this.call, "CallSource");
    logChild(this.call, "ClosedFlag");
    logChild(this.call, "CanceledFlag");
    logChild(this.call, "CreateDateTime");

    parseCall(this.call);
    // printAll(this.call);
  }


  private void logChild(Element parent, String name) {
    Element child = parent.getFirstChildElement(name, NAMESPACE);
    if (child == null) {
      sLogger.debug("parent's " + name + " = " + child);
    } else {
      String value = child.getValue();
      sLogger.debug("parent's " + name + " = " + value);
    }
  }


  private void parseCall(Element call) {
    this.callId           = getValue(call, "CallId", null);
    this.callNumber       = getValue(call, "CallNumber", null);
    this.closedFlag       = getValue(call, "ClosedFlag", null);
    this.canceledFlag     = getValue(call, "CanceledFlag", null);
    this.createDateTime   = getValue(call, "CreateDateTime", null);
    this.natureOfCall     = getValue(call, "NatureOfCall", null);
    Element tmp = call.getFirstChildElement("Location", NAMESPACE);
    if (tmp == null) {
      sLogger.warn("No location element found for callId " + this.callId);
    } else {
      this.location = new Location(tmp);
    }
  }


  private static String getValue(Element parent, String childName, String defaultValue) {
    String result = defaultValue;
    Element child = parent.getFirstChildElement(childName, NAMESPACE);
    if (child != null) {
      result = child.getValue();
    } else {
      sLogger.debug("No child found with name " + childName);
    }
    sLogger.debug("result = " + result);
    return result;
  }


  private void printAll(Element elem) {
    System.out.println("Call ID = " + this.callId);
    printAll(elem, 0);
  }


  private void printAll(Element elem, int depth) {
    String prefix = "";
    for(int j = 0; j < depth; j++) {
      prefix += " . ";
    }
    System.out.println(prefix + "elem = " + elem);
    Elements children = elem.getChildElements();
    for(int j = 0; j < children.size(); j++) {
      printAll(children.get(j), depth + 1);
    }
  }


  public static class Location {
    protected String commonName = null;
    protected String fireQuadrant = null;
    protected String policeBeat = null;
    protected String venue = null;
    protected double longitude = Double.NaN;
    protected double latitude = Double.NaN;
    protected String nearestCrossStreets = null;

    public Location(Element elem) {
      if (elem == null)
        throw new NullPointerException("element is null");
      this.commonName = getValue(elem, "CommonName", this.commonName);
      this.fireQuadrant = getValue(elem, "FireQuadrant", this.fireQuadrant);
      this.policeBeat = getValue(elem, "PoliceBeat", this.policeBeat);
      this.venue = getValue(elem, "Venue", this.venue);
      getLonLat(elem); // .getFirstChildElement("LatLonDescription", NAMESPACE));
      this.nearestCrossStreets = getValue(elem, "NearestCrossStreets", this.nearestCrossStreets);
    }


    private void getLonLat(Element location) {

      String name = "LongitudeX";
      String tmp = getValue(location, name, null);
      if (tmp != null) {
        try {
          this.longitude = Double.parseDouble(tmp);
        } catch (NumberFormatException ex) {
          sLogger.warn("Bad " + name + ": " + tmp);
          this.longitude = Double.NaN;
        }
      }

      name = "LatitudeY";
      tmp = getValue(location, name, null);
      if (tmp != null) {
        try {
          this.latitude = Double.parseDouble(tmp);
        } catch (NumberFormatException ex) {
          sLogger.warn("Bad " + name + ": " + tmp);
          this.latitude = Double.NaN;
        }
      }
    }


    public String toString() {
      return "{commonName=" + this.commonName
          + String.format(", lon/lat=%.6f/%.6f", this.longitude, this.latitude)
          + ", venue=" + this.venue
          + ", policeBeat=" + this.policeBeat
          + ", fireQuadrant=" + this.fireQuadrant
          + ", nearestCrossStreets=" + this.nearestCrossStreets
          + "}"
        ;
    }

  }


}
