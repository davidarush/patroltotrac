package wyoroad.patrolToTrac;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import wyoroad.wydgets.DarUtils;
import wyoroad.wydgets.Properties;
import wyoroad.wydata.PatrolEvent;
import wyoroad.wydata.TracTask;
import java.util.List;
import java.util.ArrayList;
import wyoroad.wydgets.WebService;
import java.net.URL;
import java.io.File;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONTokener;
import org.json.JSONException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.http.StatusLine;

import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;

//import org.apache.commons.io.ReaderInputStream;
import org.apache.commons.io.IOUtils;

import java.security.cert.X509Certificate;
import java.security.cert.CertificateException;

import java.net.URLDecoder;
import java.net.URI;
import java.util.Set;
import java.util.HashSet;
import wyoroad.wydgets.FileStuff;
import wyoroad.wydgets.DatabaseReader;
import wyoroad.wydgets.DarUtils;
import wyoroad.scrambler.Scrambler;
import wyoroad.scrambler.CipherText;
import wyoroad.scrambler.PlainText;
import org.apache.log4j.Logger;
import wyoroad.wydgets.sql.SqlSelect;
import wyoroad.wydgets.sql.SqlTableName;
import wyoroad.wydgets.sql.SqlSequenceName;
import wyoroad.wydgets.KeyGenerator;
import java.util.Map;
import java.util.HashMap;
import wyoroad.wydgets.Whenstamp;
import java.net.URLEncoder;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.StringReader;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.ContentType;
import wyoroad.wydata.WatchDogHeartBeat;
import java.sql.ResultSet;

public class PurgeOldEvents {
  private static final Logger sLogger = Logger.getLogger(PurgeOldEvents.class);
  private static final String ENCODING = "UTF-8";


  public static void main(String[] args) {
    try {
      sLogger.info("\n\nmain() starting");
      // Look at the first command line argument.  Is it a URL?  If not, assume it's a file.
      PurgeOldEvents t = null;
      t = new PurgeOldEvents();
      t.go();
    } catch (Exception ex) {
      sLogger.error("Caught in main: ", ex);
      ex.printStackTrace(System.err);
      System.exit(1);
    }
    System.exit(0);
  }


  protected SqlTableName eventsTableName = null;
  protected SqlSequenceName sequenceName = null;
  protected SqlSelect eventsSelect = null;

  protected String dbUrl = null;
  protected String dbUsername = null;
  protected PlainText dbPassword = null;
  protected Properties props = null;


  public PurgeOldEvents() throws Exception {
    // Will pull URI from properties file later.
    build();
  }


  protected void build() throws Exception {
    this.props = new Properties(new File("patrolToTrac.properties"));
    this.dbUrl = props.get("db_url");
    this.dbUsername = props.get("db_username");
    this.dbPassword = new PlainText(Scrambler.Decrypt(new CipherText(props.get("db_password"))));
    this.eventsSelect = new SqlSelect(props.get("patrol_events_select"));
    this.eventsTableName = new SqlTableName(props.get("patrol_events_table_name"));
    // this.sequenceName = new SqlSequenceName(props.get("patrol_events_sequence_name"));
  }




  public void go() throws Exception {
    // Open reader
    // Get all patrolEvents
    // For each one, if age is > 24 hours, remove it from the DB
    Whenstamp start = new Whenstamp();
    // Grab "old" events from the DB
    DatabaseReader reader = null;
    try {
      reader = getReader(props);
      List<PatrolEvent> oldEvents = getOldEvents(reader, this.eventsSelect);
      int startCount = oldEvents.size();
      System.out.println("Read " + oldEvents.size() + " events from database: " + this.eventsSelect + " as user " + this.dbUsername);
      int count = purgeOldEvents(reader, oldEvents);
      System.out.println("Purged " + count + " old events.");
      oldEvents = getOldEvents(reader, this.eventsSelect);
      System.out.println("Had " + startCount + " events, now have " + oldEvents.size() + " events from database: " + this.eventsSelect + " as user " + this.dbUsername);
    } finally {
      DarUtils.close(reader);
    }
    Whenstamp end = new Whenstamp();
    System.out.println("Started at " + start + ", ended at " + end + ", elapsed time " + start.getAgeSecs(end) + " seconds.");
  }


  private DatabaseReader getReader(Properties props) throws Exception {
    DatabaseReader reader = null;
    //String dbUrl = props.get("db_url");
    sLogger.debug("dbUrl = " + this.dbUrl);

    //String dbUsername = props.get("db_username");
    sLogger.debug("dbUsername = " + this.dbUsername);

    //PlainText dbPassword = new PlainText(Scrambler.Decrypt(new CipherText(props.get("db_password"))));
    // sLogger.debug("dbPassword = " + dbPassword);

    reader = new DatabaseReader(this.dbUrl, this.dbUsername, this.dbPassword);
    reader.setTracer("PurgeOldEvents-getEvents");
    reader.open();
    return reader;
  }


  private static final boolean REALLY_PURGE = true;


  /**
    For each newEvent, look to see if it exists in oldEvents, and if it does see if it needs updating.
    If it's new or needs updating, it creates a new TracTask for it.
   */
  public int purgeOldEvents(DatabaseReader reader, List<PatrolEvent> oldEvents) throws Exception {
    if (reader == null) throw new NullPointerException("reader is null");
    if (!(reader.isOpen())) throw new Exception("reader is not open");
    if (oldEvents == null) throw new NullPointerException("oldEvents is null");
    int purgedCount = 0;
    int j = 0;
    Whenstamp cutoff = getCutoff();  // get date/time before which events will be deleted (purged)
    System.out.println("now    = " + new Whenstamp());
    System.out.println("cutoff = " + cutoff);
    int purgeMax = oldEvents.size() / 2;
    if (oldEvents.size() % 2 > 0) {
      purgeMax++;
    }
    int min = 10;
    if (purgeMax < min) {
      purgeMax = min;
    }
    if (true) {
      purgeMax = 100000; // 
    }
    if (REALLY_PURGE) {
      for(PatrolEvent oldEvent: oldEvents) {
        Whenstamp latest = getLatest(oldEvent);
        if (latest.isBefore(cutoff)) {
          if (purgedCount < purgeMax) {
            System.out.println("Purging: " + oldEvent.getEvent() + " " + latest);
            oldEvent.deleteRow(reader, this.eventsTableName);
            purgedCount++;
          } else {
            System.out.println("Should purge: " + oldEvent.getEvent() + " " + latest);
          }
        } else {
          // System.out.println("Should keep:  " + oldEvent.getCreated());
        }
      }
    }
    return purgedCount;
  }


  /**
    return the latest (most recent) of either the created or updated timestamps.
   */
  private Whenstamp getLatest(PatrolEvent event) {
    Whenstamp latest = event.getCreated();
    if (event.getUpdated() != null) {
      latest = event.getUpdated();
    }
    return latest;
  }


  private static final int MINUTES_PER_HOUR = 60;
  private static final int SECONDS_PER_MINUTE = 60;
  private static final int SECONDS_PER_HOUR = SECONDS_PER_MINUTE * MINUTES_PER_HOUR;
  private static final int MS_PER_SECOND = 1000;
  private static final int MS_PER_HOUR = SECONDS_PER_HOUR * MS_PER_SECOND;

  private Whenstamp getCutoff() {
    final int MAX_AGE_HOURS = 48;
    // final int MAX_AGE_MINUTES = MAX_AGE_HOURS * 60;
    // final long MAX_AGE_SECONDS = MAX_AGE_MINUTES * 60L;
    final long MAX_AGE_MS = MS_PER_HOUR * MAX_AGE_HOURS;
    Whenstamp cutoff = new Whenstamp(); // start out with now
    cutoff.inc(-1 * MAX_AGE_MS);
    return cutoff;
  }



  private List<PatrolEvent> getOldEvents(DatabaseReader pReader, SqlSelect pSelect) throws Exception {
    List<PatrolEvent> events = PatrolEvent.getMany(pReader, pSelect);
    if (true) {
      sLogger.debug("Found " + events.size() + " database events.");
    }
    return events;
  }


}
