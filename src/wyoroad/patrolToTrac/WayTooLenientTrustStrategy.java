package wyoroad.patrolToTrac;

import org.apache.http.conn.ssl.TrustStrategy;
import java.security.cert.X509Certificate;
import java.security.cert.CertificateException;

public class WayTooLenientTrustStrategy implements TrustStrategy {
  @Override public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
    return true;
  }
}
