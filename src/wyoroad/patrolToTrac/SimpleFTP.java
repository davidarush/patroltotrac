package wyoroad.patrolToTrac;
// import wyoroad.wydgets.DarUtils;
// import wyoroad.scrambler.Scrambler;
// import wyoroad.scrambler.CipherText;
import wyoroad.scrambler.PlainText;
// import wyoroad.wydgets.Whenstamp;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.io.IOException;
import java.io.FileOutputStream;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.log4j.Logger;


/**
  SimpleFTP.java
  <br>Copyright 2020 State of Wyoming
  <br>Created at 2020 Jan 16
  @author David A. Rush
  */

public class SimpleFTP {
  private static final Logger sLogger = Logger.getLogger(SimpleFTP.class);
  protected FTPClient client = null;
  protected boolean useRemotePassive = false;


  /**
    Connect to given server and login with given credentials.
    */
  public SimpleFTP(String host, String username, PlainText password) throws IOException {
    sLogger.debug("Connecting to " + host + " as " + username);
    this.client = new FTPClient();
    /*
    FTPClientConfig config = new FTPClientConfig();
    config.setXXX(YYY); // change required options
    // for example config.setServerTimeZoneId("Pacific/Pitcairn")
    ftp.configure(config );
    */

    this.client.connect(host);

    // After connection attempt, you should check the reply code to verify success.
    int reply = this.client.getReplyCode();
    sLogger.debug("reply code = " + reply);
    sLogger.debug("reply string = " + this.client.getReplyString());

    if (!FTPReply.isPositiveCompletion(reply)) {
      sLogger.error("Connection failed to " + host + " because " + this.client.getReplyString());
      throw new IOException("Connection refused to " + host + " because " + this.client.getReplyString());
    } 

    if (!(this.client.login(username, password.toString()))) {
      String msg = "Failed to login: " + this.client.getReplyString();
      sLogger.debug(msg);
      throw new IOException(msg);
    } 
    sLogger.debug("Logged in to " + host + " as " + username + ", STAT = " + this.client.getStatus());

    sLogger.debug("useRemotePassive = " + useRemotePassive);
    if (this.useRemotePassive) {
      if (!(this.client.enterRemotePassiveMode())) {
        String msg = "Failed to enter remote passive mode: " + this.client.getReplyString();
        sLogger.error(msg);
        throw new IOException(msg);
      } else {
        sLogger.debug("Entered passive mode");
      }
    }
 }


  public void disconnect() throws IOException {
    this.client.logout();
    this.client.disconnect();
    sLogger.debug("Disconnected");
    this.client = null;
  }


  public boolean changeWorkingDirectory(String path) throws IOException {
    return this.client.changeWorkingDirectory(path);
  }


  public FTPFile[] listFiles(String path) throws IOException {
    return this.client.listFiles(path);
  }

  public boolean retrieveFile(String path, File file) throws IOException {
    // FTPClient is open and successfully connected
    String full = path + File.separator + file.getName();
    return retrieveFileFull(full, file);
  }


  /**
    Get a file from the current working directory.
    */
  public boolean retrieveFile(File file) throws IOException {
    // FTPClient is open and successfully connected
    String full = file.getName();
    return retrieveFileFull(full, file);
  }


  public boolean retrieveFileFull(String fullPath, File outFile) throws IOException {
    boolean result = false;
    FileOutputStream outStream = new FileOutputStream(outFile);
    try {
      result = this.client.retrieveFile(fullPath, outStream);
    } finally {
      if (outStream != null) {
        outStream.close();
      }
    }
    sLogger.debug("result of " + result + " retrieving file " + fullPath);
    return result;
  }


  /**
    Retrieve all the files in the current working directory.
    */
  public boolean retrieveAllFiles() throws IOException {
    boolean result = false;
    String[] fileNames = this.client.listNames();
    int n = 0;
    for(String fileName: fileNames) {
      sLogger.debug(String.format("%4d: %s", n++, fileName));
    }
    return result;
  }




}
