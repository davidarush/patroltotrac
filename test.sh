#!/bin/bash
ant clean
ant jar
if [ $? != 0 ]; then
  echo Build failed.  Bummer.
else
  #./PatrolToTrac2.sh sample_20160429a.json
  #./PatrolToTrac2.sh sample_20160516a.json
  #./PatrolToTrac2.sh sample_20160516b.json
  #./PatrolToTrac2.sh sample_20160517a.json
  #./PatrolToTrac2.sh sample_20160517b.json
  #./PatrolToTrac2.sh sample_20160517c.json
  #./PatrolToTrac2.sh sample_20160525a.json
  #./PatrolToTrac2.sh sample_20160525b.json
  #./PatrolToTrac2.sh sample_20160525c.json
  #./PatrolToTrac2.sh sample_20160525d.json
  #./PatrolToTrac2.sh sample_20160525e.json
  #./PatrolToTrac2.sh sample_20160525f.json
  #./PatrolToTrac2.sh sample_20160603b.json
  ./PatrolToTrac2.sh sample_20160609_053711.json
  if [ $? != 0 ]; then
    echo PatrolToTrac2.sh failed.  Bummer.
  else
    # ant selects
    cat patrolToTrac.log
  fi
fi
