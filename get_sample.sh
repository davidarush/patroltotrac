#!/bin/bash
if [ ! -f /mnt/iiasp01/tasks/test_patrol/sample.json ]; then
  echo Source file not found: /mnt/iiasp01/tasks/test_patrol/sample.json
  exit 1
fi
if [ -f sample.json ]; then
  echo sample.json already exists.  Rename or delete it.
  exit 1
fi
cp /mnt/iiasp01/tasks/test_patrol/sample.json .
node sanitizeSample.js </mnt/iiasp01/tasks/test_patrol/sample.json >sample.json
