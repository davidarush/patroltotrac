@echo off
pushd d:\tasks\patrol
set CP=patrolToTrac.jar
set CP=%CP%;.
set CP=%CP%;wydgets.jar
set CP=%CP%;scrambler.jar
set CP=%CP%;wydata.jar
set CP=%CP%;log4j-1.2.17.jar
set CP=%CP%;commons-codec-1.9.jar
set CP=%CP%;ojdbc6.jar
set CP=%CP%;commons-net-3.3.jar
set CP=%CP%;json.jar
set CP=%CP%;httpclient-4.5.1.jar
set CP=%CP%;httpcore-4.4.3.jar
set CP=%CP%;commons-logging-1.2.jar
REM # add current dir so it can find .properties files.
set CP=%CP%;.
java -cp %CP% wyoroad.patrolToTrac.Test $*
popd
